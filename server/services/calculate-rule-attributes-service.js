import {ApplicationService} from "@themost/common";

const RULES_REFERENCES = [
    {
        "refersToIdentifier": 10,
        "refersTo": "Student"
    },
    {
        "refersToIdentifier": 20,
        "refersTo": "CourseArea"
    },
    {
        "refersToIdentifier": 30,
        "refersTo": "Course"
    },
    {
        "refersToIdentifier": 40,
        "refersTo": "CourseType"
    },
    {
        "refersToIdentifier": 50,
        "refersTo": "ProgramGroup"
    },
    {
        "refersToIdentifier": 60,
        "refersTo": "Thesis"
    },
    {
        "refersToIdentifier": 70,
        "refersTo": "Internship"
    },
    {
        "refersToIdentifier": 80,
        "refersTo": "MeanGrade"
    },
    {
        "refersToIdentifier": 90,
        "refersTo": "YearMeanGrade"
    },
    {
        "refersToIdentifier": 100,
        "refersTo": "RegisteredCourse"
    },
    {
        "refersToIdentifier": 110,
        "refersTo": "CourseCategory"
    },
    {
        "refersToIdentifier": 120,
        "refersTo": "CourseSector"
    },
    {
        "refersToIdentifier": 130,
        "refersTo": "SectionCategory"
    },
];

const RULES_TYPES = [
    {
        "targetTypeIdentifier": 101,
        "additionalTypeIdentifier": 1,
        "targetType": "Program",
        "additionalType": "ProgramInscriptionRule"
    },
    {
        "targetTypeIdentifier": 102,
        "additionalTypeIdentifier": 1,
        "targetType": "Program",
        "additionalType": "SpecialtyRule"
    },
    {
        "targetTypeIdentifier": 103,
        "additionalTypeIdentifier": 1,
        "targetType": "Program",
        "additionalType": "GraduateRule"
    },
    {
        "targetTypeIdentifier": 103,
        "additionalTypeIdentifier": 8,
        "targetType": "Student",
        "additionalType": "GraduateRule"
    },
    {
        "targetTypeIdentifier": 104,
        "additionalTypeIdentifier": 1,
        "targetType": "Program",
        "additionalType": "ThesisRule"
    },
    {
        "targetTypeIdentifier": 105,
        "additionalTypeIdentifier": 1,
        "targetType": "Program",
        "additionalType": "InternshipRule"
    },
    {
        "targetTypeIdentifier": 106,
        "additionalTypeIdentifier": 4,
        "targetType": "ProgramCourse",
        "additionalType": "ProgramCourseRegistrationRule"
    },
    {
        "targetTypeIdentifier": 106,
        "additionalTypeIdentifier": 14,
        "targetType": "CourseClass",
        "additionalType": "ClassRegistrationRule"
    },
    {
        "targetTypeIdentifier": 107,
        "additionalTypeIdentifier": 3,
        "targetType": "ProgramGroup",
        "additionalType": "GroupRegistrationRule"
    },
    {
        "targetTypeIdentifier": 111,
        "additionalTypeIdentifier": 14,
        "targetType": "CourseClass",
        "additionalType": "SectionRegistrationRule"
    },
    {
        "targetTypeIdentifier": 109,
        "additionalTypeIdentifier": 13,
        "targetType": "Scholarship",
        "additionalType": "ScholarshipRule"
    },
    {
        "targetTypeIdentifier": 112,
        "additionalTypeIdentifier": 1,
        "targetType": "Program",
        "additionalType": null
    },
]

/**
 * @class
 */
class CalculateRuleAttributesService extends ApplicationService {

    /**
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);

        this.references = RULES_REFERENCES;
        this.types = RULES_TYPES;

        this.optionalCheckValues = ["CourseType", "Thesis"]
    }

}

module.exports.CalculateRuleAttributesService = CalculateRuleAttributesService;