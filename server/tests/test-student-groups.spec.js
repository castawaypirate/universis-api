import {assert} from 'chai';
import app from '../app';
import Student from '../models/student-model';
import {TestUtils} from '../utils';


describe('test available student groups', () => {

    /**
     * @type ExpressDataContext
     */
    let context;
    before(done => {
        /**
         * @type ExpressDataApplication
         */
        const _app = app.get('ExpressDataApplication');
        // create context
        context = _app.createContext();
        // set current student
        // user with success: 'user_20100248@example.com'
        // user with success and complex rules: 'user_20131066@example.com'
        // user failure: 'user_20138601@example.com'
        context.user = {
            name: 'user_20118494@example.com'
        };
        return done();
    });
    after(done => {
        if (context == null) {
            return done();
        }
        context.finalize(() => {
            return done();
        });
    });

    it('should get all available student groups', async ()=> {
        /**
         * @type Student
         */
        const student = await Student.getMe(context).getTypedItem();
        const data = {
            student: student
        };
        const availableStudentGroups =await student.availableProgramGroups();
        assert.isArray(availableStudentGroups);

    });

    it('should add get power of two', async () => {
        let x = 255;
        let powers = getPowersOfTwo(x);
        const minNumber = powers.reduce((prev,curr)=>{
            return Math.min(prev,curr)
        });
        console.log (minNumber+1);

    });

    function getPowersOfTwo(value) {
        let b = 1;
        let res = [];
        while (b <= value) {
            if (b & value) {
                res.push(getPower(2, b));
            }
            b <<= 1;
        }
        return res;
    }
    function getPower(x, y)
    {
       // Repeatedly compute power of x
        let pow = 1;
        let power =0;
        while (pow < y) {
            pow = pow * x;
            power +=1;
        }
        // Check if power of x becomes y
        if (pow===y)
            return power;

    }

    it('should add student group selection action', async () =>  {

        /**
         * @type Student
         */
        let student = await context.model('Student').where('user/name').equal(context.user.name).getTypedItem();
     //   await TestUtils.executeInTransaction(context, async () => {
            // get available course exams
            const availableGroups =await student.availableProgramGroups();

            const studentGroupAction ={
                object: student.id,
                groups:[]
            };
            availableGroups.forEach(group=>{
               let i = 0;
               group.groups.forEach(programGroup=>{
                   if (i<group.minCourses) {
                       studentGroupAction.groups.push(programGroup);
                       i +=1;
                   }
               });

            });
            // save register action
            await context.model('UpdateStudentGroupAction').save(studentGroupAction);

       // });
    });
});
