import {assert} from 'chai';
import app from '../app';
import {TestUtils} from '../utils';
import moment from 'moment';
import Student from '../models/student-model';

describe('student available classes', () => {

    /**
     * @type ExpressDataContext
     */
    let context;
    before(done => {
        /**
         * @type ExpressDataApplication
         */
        const _app = app.get('ExpressDataApplication');
        // create context
        context = _app.createContext();
        // set current student
        context.user = {
            name: 'user_20132163@example.com'
        };
        return done();
    });
    after(done => {
        if (context == null) {
            return done();
        }
        context.finalize(() => {
            return done();
        });
    });

    it('get available classes', async ()=> {
        const student = await Student.getMe(context).getTypedItem();
        const availableClasses = await (await student.getAvailableClasses()).getItems();
        assert.equal(availableClasses.length, 0);
    });



});
