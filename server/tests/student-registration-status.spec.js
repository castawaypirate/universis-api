import {assert} from 'chai';
import app from '../app';
import {TestUtils} from '../utils';
import moment from 'moment';

describe('student registration status', () => {

    /**
     * @type ExpressDataContext
     */
    let context;
    before(done => {
        /**
         * @type ExpressDataApplication
         */
        const _app = app.get('ExpressDataApplication');
        // create context
        context = _app.createContext();
        // set current student
        context.user = {
            name: 'user_20132907@example.com'
        };
        return done();
    });
    after(done => {
        if (context == null) {
            return done();
        }
        context.finalize(() => {
            return done();
        });
    });

    it('check INACTIVE_STUDENT', async ()=> {
        /**
         * get inactive student
         * @type {Student}
         */
        const student = await context.model('Student').where('studentStatus/alternateName').equal('graduated').silent().getTypedItem();
        assert.isObject(student);
        const status = await student.silent().getCurrentRegistrationStatus();
        assert.isObject(status);
        assert.equal(status.code, 'INACTIVE_STUDENT');
    });

    it('check CLOSED_REGISTRATION_PERIOD', async ()=> {
        await TestUtils.executeInTransaction(context,async ()=> {
            // get first local department
            const department = await context.model('LocalDepartment').asQueryable()
                .select('id','registrationPeriodStart', 'registrationPeriodEnd')
                .silent()
                .orderByDescending('dateModified')
                .getItem();
            assert.isObject(department);
            // change department period
            department.registrationPeriodStart = moment(new Date()).subtract(14, 'day').startOf('day').toDate();
            department.registrationPeriodEnd = moment(new Date()).subtract(7, 'day').startOf('day').toDate();
            await context.model('LocalDepartment').silent().save(department);
            // get an active student from this department
            const student = await context.model('Student')
                .where('studentStatus/alternateName').equal('active')
                .and('department').equal(department.id).silent().getTypedItem();
            assert.isObject(student);
            const status = await student.silent().getCurrentRegistrationStatus();
            assert.isObject(status);
            assert.equal(status.code, 'CLOSED_REGISTRATION_PERIOD');
        });
    });

    it('check OPEN_NO_TRANSACTION', async ()=> {
        context.user = {
            name: 'registrar1@example.com'
        };
        await TestUtils.executeInTransaction(context,async ()=> {
            // get first local department
            const department = await context.model('LocalDepartment').asQueryable()
                .select('id','registrationPeriodStart', 'registrationPeriodEnd')
                .silent()
                .orderByDescending('dateModified')
                .getTypedItem();
            assert.isObject(department);
            // change department period
            department.registrationPeriodStart = moment(new Date()).subtract(14, 'day').startOf('day').toDate();
            department.registrationPeriodEnd = moment(new Date()).subtract(-14, 'day').startOf('day').toDate();
            await context.model('LocalDepartment').silent().save(department);
            // get current year period
            const currentYear = await department.property('currentYear').select('id').value();
            const currentPeriod = await department.property('currentPeriod').select('id').value();
            // get an active student from this department
            const data = await context.model('StudentPeriodRegistration')
                .where('student/studentStatus/alternateName').equal('active')
                .and('student/department').equal(department.id)
                .select('student','max(registrationYear) as registrationYear', 'max(registrationPeriod) as registrationPeriod')
                .silent()
                .take(100)
                .groupBy('student')
                .getItems();
            assert.isArray(data);
            const findStudent = data.find( x=> {
                return x.registrationYear < currentYear && x.registrationPeriod <= currentPeriod;
            });
            assert.isObject(findStudent);
            const student = context.model('Student').convert({
                id: findStudent.student
            });
            const status = await student.silent().getCurrentRegistrationStatus();
            assert.isObject(status);
            assert.equal(status.code, 'OPEN_NO_TRANSACTION');
        });
    });

});
