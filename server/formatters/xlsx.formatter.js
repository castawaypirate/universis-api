import {HttpResponseFormatter} from "@themost/express";

export class XlsxFormatter extends HttpResponseFormatter {
    execute(req, res) {
        if (this.data && this.data.value && Array.isArray(this.data.value))
            return res.xls(this.data.value);
        else
            return res.xls(this.data);
    }
}