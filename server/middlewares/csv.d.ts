import {RequestHandler} from "express";

declare interface FastCsvOptions {
    delimiter?: string;
    headers?: boolean;
    rowDelimiter?: string;
    transform?: Function;
}

declare interface PostCsvOptions {
    name: string;
    csv?: FastCsvOptions;
}

export declare function csvParser(options?: FastCsvOptions): RequestHandler;
export declare function csvPostParser(options?: PostCsvOptions): RequestHandler;