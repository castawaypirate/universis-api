import _ from 'lodash';
import {DataObject} from '@themost/data/data-object';
import {DataNotFoundError} from "@themost/common/errors";
import {HttpServerError, HttpBadRequestError} from "@themost/common/errors";
import {RandomUtils, TraceUtils} from "@themost/common/utils";
import {PrivateContentService} from "../services/content-service";
import path from "path";
import pdf from  "html-pdf";
import util from "util";
import fs from "fs";
const AttachmentModel = require('../models/attachment-model');


export class MimeTypeError extends HttpBadRequestError {
    constructor(message) {
        super(message);
        this.code = "EMIME";
    }
}

class EnableAttachmentModel extends DataObject {

    constructor() {
        super();
    }

    /**
     * @returns {Promise<*>}
     */
    getRecipient() {
       if (this.getModel().getAttribute('recipient')) {
           return this.getModel().where('id').equal(this.id).select('recipient').silent().value();
       }
       return Promise.resolve();
    }

    addAttachment(file) {
        const context = this.context;
        const self = this;
        let finalResult;
        return new Promise((resolve, reject)=> {
            this.context.db.executeInTransaction((callback)=> {
                file.name = AttachmentModel.convertFileName(file);
                const mime = file.mimetype || file.contentType;
                if (_.isNil(mime)) {
                    return reject(new MimeTypeError('The specified file type is not supported by the system.'))
                }
                const addAttributes ={};
                _.forEach(context.model('Attachment').attributeNames, (attribute)=> {
                    if (file.hasOwnProperty(attribute)) {
                        addAttributes[attribute] = file[attribute];
                    }
                });
                const newAttachment = _.assign(addAttributes, { contentType:mime });
                const svc = context.getApplication().getService(PrivateContentService);
                    //add attachment to attachments
                    context.unattended((cb)=> {
                        // set attachment ownership to recipient
                        self.getRecipient().then( recipient => {
                            if (recipient) {
                                newAttachment.owner = recipient;
                            }
                            let filePath;
                            if (file.destination && file.filename) {
                                filePath = path.resolve(file.destination, file.filename);
                            } else {
                                filePath = file.path;
                            }
                            svc.copyFrom(context,  filePath, newAttachment, function(err) {
                                if (err) {
                                    TraceUtils.error(err);
                                    return cb(new HttpServerError());
                                }
                                return cb();
                            });
                        }).catch( err=> {
                            return cb(err);
                        });

                    }, (err)=> {
                        if (err) {
                            return callback(err);
                        }
                        const attachments = self.property('attachments');
                        // set attachment ownership
                        attachments.insert(newAttachment, (err)=> {
                            if (err) {
                                return callback(err);
                            }
                            svc.resolveUrl(context, newAttachment, function(err, url) {
                                if (err) {
                                    callback(err);
                                }
                                else {
                                    finalResult =_.assign({
                                    }, newAttachment, {
                                        url: url,
                                        name: newAttachment.filename
                                    });
                                    callback();
                                }
                            });
                        });
                    });

            }, (err)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(finalResult);
            });
        })

    }

    removeAttachment(id) {
        const context = this.context;
        const self = this;
        let result;
        return new Promise((resolve, reject)=> {
            self.context.db.executeInTransaction((callback)=> {
                //get attachment
                return context.model('Attachment').where('id').equal(id).getItem().then((attachment)=> {
                   if (attachment) {
                       //remove attachment connection
                       return self.property('attachments').remove(attachment,(err)=> {
                           //remove attachment
                           if (err) {
                               return callback(err);
                           }
                           return context.model('Attachment').silent().remove(attachment).then(()=> {
                               result = attachment;
                               return callback();
                           }).catch((err)=> {
                               return callback(err);
                           });
                       });
                   }
                   return callback(new DataNotFoundError());
                }).catch((err)=> {
                    return callback(err);
                });
            }, (err)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(result);
            });
        });
    }

   async addHtmlAsAttachment(data) {

       const self = this;
       //generate pdf
       let options = {
           format: 'A4', "border": {
               "top": "0.5in",
               "right": "0.5in",
               "bottom": "0.5in",
               "left": "0.5in"
           }
       };
       const fileName = RandomUtils.randomChars(12).toUpperCase() + '.pdf';
       const tmpFile = path.resolve(process.cwd(), '.tmp/' + fileName);

       //get random file name
       const pdfCreator = pdf.create(data, options);
       const pdfCreateToFileAsync = util.promisify(pdfCreator.toFile.bind(pdfCreator));
       const file = await pdfCreateToFileAsync(tmpFile);
       // add options
       file.originalname = fileName;
       file.mimetype = 'application/pdf'

       return await this.addAttachment(file);
   }

}

module.exports=EnableAttachmentModel;
