import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @property {Thesis|any} thesis
 * @property {Student|any} student
 * @property {StudentThesis|any} studentThesis
 * @property {Instructor|any} instructor
 * @property {number} index
 * @property {number} grade
 * @property {number} factor
 * @property {number} isPassed
 * @property {string} formattedGrade
 * @property {Date} dateModified
 * @augments {DataObject}
 */
@EdmMapping.entityType('StudentThesisResult')
class StudentThesisResult extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = StudentThesisResult;