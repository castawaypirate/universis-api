import {EdmMapping, EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import {Args} from "@themost/common/utils";
import {DataConflictError} from "../errors";
let EducationEvent = require('./education-event-model');
/**
 * @class
 
 * @property {CourseClass|any} courseClass
 * @property {Array<CourseClassSection|any>} sections
 * @property {number} id
 * @property {number} eventAttendanceCoefficient
 * @augments {DataObject}
 */
@EdmMapping.entityType('TeachingEvent')
class TeachingEvent extends EducationEvent {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    @EdmMapping.action('close', 'TeachingEvent')
    async closeEvent(){
        return this.context.model('TeachingEvent').save({
            ...this,
            eventStatus: await this.context.model('EventStatusTypes').where('alternateName').equal('EventCompleted').getItem()
        });
    }

    @EdmMapping.action('open', 'TeachingEvent')
    async openEvent(){
        return this.context.model('TeachingEvent').save({
            ...this,
            eventStatus: await this.context.model('EventStatusTypes').where('alternateName').equal('EventOpened').getItem()
        });
    }

    @EdmMapping.param('records', EdmType.CollectionOf('TeachingEventAttendance'), false, true)
    @EdmMapping.action('attendance', EdmType.CollectionOf('TeachingEventAttendance'))
    async setAttendanceRecords(records) {
        const _event = await this.context.model('TeachingEvent').where('id').equal(this.getId()).expand('eventStatus').getItem();
        if(_event.hasOwnProperty('eventStatus') && _event.eventStatus.hasOwnProperty('alternateName') && _event.eventStatus.alternateName === 'EventCompleted') {
            throw new DataConflictError('You cannot add attendance records to a completed event.', null, 'TeachingEvent');
        }
        // get examOnlyClassStatus value from institute configuration to prevent saving attendance for class registration with this status
        const  examOnlyClassStatus= await this.context.model('Department').where('id').equal(_event.organizer)
            .select('organization/instituteConfiguration/examOnlyClassStatus').silent().value();
        const finalRecords = [];
        Args.check(Array.isArray(records) || typeof records === 'object', new DataConflictError('Records should either be an array or an object of records', null, 'EducationEventAttendance'));
        if (Array.isArray(records)){
            // check if every item in array has the same coefficient
            if((new Set(records.map(x => x.hasOwnProperty('coefficient') && x.coefficient))).size > 1)
                throw new DataConflictError('Not all records have the same coefficient', 'Make sure that every record has the same coefficient', 'EducationEventAttendance');
            for (let [index, record] of records.entries()) {
                if (record.hasOwnProperty('coefficient') && _event.hasOwnProperty('eventAttendanceCoefficient') && _event.eventAttendanceCoefficient !== record.coefficient) {
                    // check if all records have the same coefficient as the first element if the coefficient in the attendance record doesn't match the one in the event.
                    // checks only once to save some resources even though the check for the index is performed in every iteration, it isn't as expensive.
                    if (index === 0 && !records.every(x => x.coefficient === record.coefficient))
                        throw new DataConflictError('Coefficient values between attendance record and event don\'t match.', `Check the record coefficient of student ${record.student}`, 'TeachingEvent');
                }
                record.event = {..._event};
                // exclude record if studentCourseClass has examOnlyClassStatus and prevent saving
                if (record.$state!==4 && examOnlyClassStatus!=null) {
                    const studentCourseClass = await this.context.model('StudentCourseClass')
                        .where('id').equal(record.studentCourseClass)
                        .and('status').equal(examOnlyClassStatus)
                        .select('id')
                        .silent().getItem();
                    if (studentCourseClass) {
                        // ignore this record, do not throw error
                        continue;
                    }
                }
                finalRecords.push(record);
            }
        } else {
            // here the records is object and it is guaranteed by the Args.check() at the beginning of the method.
            if (records.hasOwnProperty('coefficient') && _event.hasOwnProperty('eventAttendanceCoefficient') && _event.eventAttendanceCoefficient !== records.coefficient) {
                throw new DataConflictError('Coefficient between attendance record and event don\'t match.', null, 'TeachingEvent');
            }
            finalRecords.push(records);
        }
        return this.context.model(_event.presenceType ===1 ? 'TeachingEventPresence' : 'TeachingEventAbsence').save(finalRecords);
    }

    @EdmMapping.func("attendance", EdmType.CollectionOf("StudentCourseClass"))
    async getAttendanceRecords() {
        const event = await this.context.model('TeachingEvent').where('id').equal(this.getId()).expand('sections').getTypedItem();
        return event.sections.length ? this.context.model('StudentCourseClass').where('courseClass').equal(event.courseClass)
                .and('section').in(event.sections.map(x => x.section))
                .expand({
                    "name": "attendances",
                    "options": {
                        "$filter": `event eq ${this.getId()}`
                    }
                }).select('TeachingEventClassStudents').prepare()
            : this.context.model('StudentCourseClass').where('courseClass').equal(event.courseClass)
                .expand({
                    "name": "attendances",
                    "options": {
                        "$filter": `event eq ${this.getId()}`
                    }
                }).select('TeachingEventClassStudents').prepare();
    }

    async addToTimetable(event, timetable) {
        const teachingEvent = await this.getModel().where('id').equal(this.getId()).expand('courseClass').getItem();
        
        if (teachingEvent && teachingEvent.courseClass) {
            if (timetable.academicPeriods && timetable.academicPeriods.find(period => period.id === teachingEvent.courseClass.period)) {
                const newCourseClass = await this.context.model('CourseClass')
                    .where('period').equal(teachingEvent.courseClass.period)
                    .and('year').equal(timetable.academicYear)
                    .and('course').equal(teachingEvent.courseClass.course)
                    .expand({
                        'name': 'instructors',
                        'options': {
                            '$select': 'id,role/alternateName as role,instructor/user as user'
                        }
                    })
                    .getItem();

                // TODO: find corresponding courseClassSections
                if (newCourseClass) {
                    const { startDate, endDate, superEvent, eventHoursSpecification = null } = event;

                    // if the same teacher is still an instructor add them as the performer, else add one of the class's supervisors
                    const samePerformer = newCourseClass.instructors.find(instructor => instructor.user === teachingEvent.performer);
                    let performer = samePerformer && samePerformer.user;
                    if (!performer) {
                        // find instructor with supervisor role
                        const supervisor = newCourseClass.instructors.find(instructor => instructor.role === 'supervisor');
                        if (!(supervisor && supervisor.user)) {
                            throw new Error('Unable to find a performer for this event.')
                        }
                        performer = supervisor.user;
                    }

                    // eslint-disable-next-line no-unused-vars
                    const { id, dateModified, dateCreated, eventStatus, ...teachingEventBase } = teachingEvent;
                    return await this.getModel().save({
                        ...teachingEventBase,
                        courseClass: newCourseClass.id,
                        about: newCourseClass.id,
                        performer,
                        eventHoursSpecification,
                        startDate,
                        endDate,
                        superEvent
                    });
                } else {
                    throw new Error('Unable to find a courseClass for this year and period.')
                }
            }
        }
    }

    getYear() {
        return this.property('courseClass').getItem().then(courseCLass => courseCLass.year)
    }

    getPeriod() {
        return this.property('courseClass').expand('period').getItem().then(courseCLass => courseCLass.period);
    }
}
module.exports = TeachingEvent;
