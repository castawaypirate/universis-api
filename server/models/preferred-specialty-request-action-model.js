import { DataObject } from '@themost/data/data-object';
import { EdmMapping, EdmType } from '@themost/data/odata';
import { DataNotFoundError } from '@themost/common';
import ActionStatusType from "./action-status-type-model";
import { ValidationResult } from '../errors';


@EdmMapping.entityType('PreferredSpecialtyRequestAction')
/**
 * @class
 * @augments {DataObject}
 */
class PreferredSpecialtyRequestAction extends DataObject {
	constructor() {
		super();
	}

    /**
    * Allows current user to re-activate a cancelled PreferredSpecialtyRequestAction
    */
    @EdmMapping.func('canActivate', EdmType.EdmBoolean)
    async canActivate() {
        // validate action status
        const item = await this.getModel()
            .where('id').equal(this.getId())
            .select('student', 'actionStatus/alternateName as actionStatus')
            .getItem();
        if (item == null) {
            throw new DataNotFoundError('The action cannot be found or is inaccessible.', null, this.getModel().name);
        }
        // only rejected actions can be re-activated
        if (item.actionStatus !== ActionStatusType.CancelledActionStatus) {
            return false;
        }
        // get current student specialty
        if (item.student == null) {
            throw new DataNotFoundError('The student cannot be found or is inaccessible.', null, this.getModel().name);
        }
        const student = this.context.model('Student').convert(item.student);
        // validate that student is active
        const isActive = await student.isActive();
        if (!isActive) {
            return false;
        }
        const currentSpecialty = await student.getSpecialty();
        // validate that the student (still) belongs to the core specialty of their study program
        if (currentSpecialty && currentSpecialty.specialty > -1) {
            return false;
        }
        return true;
    }

    @EdmMapping.action('validateSpecialtyRules', 'PreferredSpecialtyRequestAction')
    async validateSpecialtyRules() {
        let result = {};
        // get student
        const student = await this.property('student').select('id').getTypedItem();
        if (student == null) {
            throw new DataNotFoundError('The student cannot be found or is inaccessible.');
        }
        // get target specialty
        const targetSpecialty = await this.property('specialty').getItem();
        if (targetSpecialty == null) {
            throw new DataNotFoundError('The target specialty cannot be found or is inaccessible.');
        }
        // get current student specialty
        const currentSpecialty = await student.getSpecialty();
        const followsTargetSpecialty = targetSpecialty.id === (currentSpecialty && currentSpecialty.id);
        if (followsTargetSpecialty) {
            // save self
            result = await this._setCanSelectSpecialty(false);
            // append falsy validation result
            result.validationResult = new ValidationResult(false, 'E_SPECIALTY', 'The student already follows the target specialty.');
            // and return
            return result;
        }
        // validate that the student can select specialty
        const canSelectSpecialty = await student.canSelectSpecialty();
        if (!(canSelectSpecialty && canSelectSpecialty.success === true)) {
            // save self
            result = await this._setCanSelectSpecialty(false);
            // append falsy validation result
            result.validationResult = canSelectSpecialty;
            // and return
            return result;
        }
        // validate target specialty rules
        const studentSpecialty = this.context.model('StudentSpecialty').convert({
            student: student,
            specialty: targetSpecialty
        });
        const validationResult = await new Promise((resolve, reject) => {
            studentSpecialty.validate((err, validationResult) => {
                if (err) {
                    return reject(err);
                }
                return resolve(validationResult);
            })
        });
        // save self
        result = await this._setCanSelectSpecialty(validationResult && validationResult.success);
        if (result == null) {
            return;
        }
        // append validation result
        result.validationResult = validationResult;
        // and return
        return result;
    }

    async _setCanSelectSpecialty(value) {
        if (typeof value !== 'boolean') {
            return Promise.resolve();
        }
        return await this.context.model('PreferredSpecialtyRequestAction').save(
            {
                id: this.getId(),
                canSelectSpecialty: value
            }
        );
    }
}

module.exports = PreferredSpecialtyRequestAction;
