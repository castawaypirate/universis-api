import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import GradeScale = require('./grade-scale-model');

/**
 * @class
 */
declare class GradeScaleValue extends DataObject {

     
     /**
      * @description Μοναδικός κωδικός
      */
     public id: number; 
     
     /**
      * @description Το όνομα του στοιχείο.
      */
     public name?: string; 
     
     /**
      * @description Ένα αναγνωριστικό όνομα για το στοιχείο.
      */
     public alternateName?: string; 
     
     /**
      * @description Η κλίμακα βαθμολογίας με την οποία συνδέεται το στοιχείο αυτό.
      */
     public gradeScale: GradeScale|any; 
     
     /**
      * @description Το κάτω όριο της τιμής της κλίμακας βαθμολογίας.
      */
     public valueFrom: number; 
     
     /**
      * @description Το άνω όριο της τιμής της κλίμακας βαθμολογίας.
      */
     public valueTo: number; 
     
     /**
      * @description Η ακριβής τιμή της κλίμακας βαθμολογίας, εάν αυτή χρησιμοποιείται για την εισαγωγή βαθμολογίας.
      */
     public exactValue?: number; 

}

export = GradeScaleValue;