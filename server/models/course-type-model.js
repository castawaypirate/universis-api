import {EdmMapping} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import {EdmType} from "@themost/data";

/**
 * @class
 
 * @property {number} id
 * @property {string} name
 * @property {string} abbreviation
 */
@EdmMapping.entityType('CourseType')
class CourseType extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * Returns a collection of courseTypes
     * @param {DefaultDataContext} context
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('All', EdmType.CollectionOf('CourseType'))
   static async getAllCourseTypes(context) {
        const courseTypes  = await context.model('CourseType').asQueryable().getAllItems();
        const courseType = courseTypes.find(x=>{
            return x.id === -1;
        });
        if (!courseType) {
            courseTypes.unshift({"id": -1, "name": "All"});
        }
        return courseTypes;
    }
}
module.exports = CourseType;
