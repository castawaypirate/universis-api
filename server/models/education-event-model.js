import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import { Event } from '@universis/events';

function id(input) {
    if (!input) throw new Error('Input cannot be null');
    return typeof input === 'object' ? input.id : input;
}

/**
 * @class
 
 * @property {string} assesses
 * @property {EducationLevel|any} educationalLevel
 * @property {string} teaches
 * @property {number} presenceType
 * @property {Array<EducationEventAttendance|any>} attendanceList
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('EducationEvent')
class EducationEvent extends Event {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * Finds a timetable that can accommodate the event. If a timetable is already provided it validates
     * wether it is suitable. If the even is a sub-event it returns immediately.
     *
     * @param {*} event
     * @return {*} 
     * @memberof EducationEvent
     */
    async getTimetable(event) {
        if (!event) {
            event = this;
        }

        // If the event has a superEvent which is not a timetable event then the event is a sub-event. No need to continue;
        if (event.superEvent) {
            const superEvent = await this.context.model('Events').where('id').equal(id(event.superEvent)).silent().getItem();
            if (superEvent.additionalType !== 'TimetableEvent') {
                return;
            }
        }

        // get eventHoursSpecification item with properties
        if (event.eventHoursSpecification && typeof event.eventHoursSpecification !== 'object') {
            event.eventHoursSpecification = await this.context.model('EventHoursSpecification').where('id').equal(event.eventHoursSpecification).silent().getItem();
        }

        // base query
        const eventEndDate = new Date(new Date(event.eventHoursSpecification ? event.eventHoursSpecification.validThrough : event.endDate).setHours(0,0,0,0))
        const timetableQuery = this.context.model('TimetableEvent')
            .where('eventStatus/alternateName').equal('EventOpened')
            .and('organizer').equal(event.organizer)
            .and('startDate').lowerOrEqual(event.eventHoursSpecification ? event.eventHoursSpecification.validFrom : event.startDate)
            .and('endDate').greaterOrEqual(eventEndDate)
            .and('availableEventTypes/alternateName').equal(event.additionalType)
            .orderByDescending('startDate');

        // get typed event and extend the query to ensure that the timetable can accommodate the event 
        // (has same academicYear, includes the provided location, etc. )
        const typedEvent = this.context.model(event.additionalType).convert(event);

        if ('getYear' in typedEvent) {
            const year = await typedEvent.getYear();
            if (year) {
                timetableQuery.and('academicYear/id').equal(id(year));
            }
        }

        if ('getPeriod' in typedEvent) {
            const period = await typedEvent.getPeriod();
            if (period) {
                timetableQuery.and('academicPeriods/id').equal(id(period));
            }
        }

        if ('getExamPeriod' in typedEvent) {
            const period = await typedEvent.getExamPeriod();
            if (period) {
                timetableQuery.and('examPeriods/id').equal(id(period));
            }
        }

        if (typedEvent.location) {
            timetableQuery.and('availablePlaces/id').equal(id(typedEvent.location));
        }

        if (typedEvent.superEvent) {
            timetableQuery.and('id').equal(id(typedEvent.superEvent))
        }

        const timetable = await timetableQuery.silent().getItem();
        if (!timetable) {
            throw new Error('Could not find a timetable that can accommodate this event.')
        }

        return timetable;
    }
}
module.exports = EducationEvent;
