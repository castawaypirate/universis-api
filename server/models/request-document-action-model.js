import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import {DataNotFoundError} from "@themost/common";
import ActionStatusType from "./action-status-type-model";
import {DataPermissionEventListener, PermissionMask} from "@themost/data";
import {promisify} from "util";
let StudentRequestAction = require('./student-request-action-model');
/**
 * @class
 
 * @property {number} documentCopies
 * @property {Action|any} initiator
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('RequestDocumentAction')
class RequestDocumentAction extends StudentRequestAction {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * Allows current user to activate again a completed or rejected action
     */
    @EdmMapping.func('canActivate', EdmType.EdmBoolean)
    async canActivate() {
        // validate action status
        const item = await this.getModel()
            .where('id').equal(this.getId())
            .select('id','actionStatus','result')
            .expand('result')
            .getItem();
        if (item == null) {
            throw new DataNotFoundError('Action not found or is inaccessible.', null, this.getModel().name);
        }
        // only completed or rejected actions can be re-activated
        if (item.actionStatus.alternateName !== ActionStatusType.CompletedActionStatus && item.actionStatus.alternateName !== ActionStatusType.CancelledActionStatus) {
            return false;
        }
        // validate action/CanActivate execute permission
        const event = {
            model: this.getModel(),
            privilege: `${this.getModel().name}/CanActivate`,
            mask: PermissionMask.Execute,
            target: item,
            throwError: false
        };
        const validator = new DataPermissionEventListener();
        // noinspection JSUnresolvedFunction
        const validateAsync = promisify(validator.validate)
        await validateAsync(event);
        if (event.result === false) {
            return false;
        }
        if (item.result == null || (item.result && item.result.published===false)) {
            return true;
        }
        return false;
    }
}
module.exports = RequestDocumentAction;
