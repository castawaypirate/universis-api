import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @property {string} additionalType
 * @property {string} alternateName
 * @property {string} contentType
 * @property {Date} datePublished
 * @property {boolean} published
 * @property {string} keywords
 * @property {string} thumbnail
 * @property {string} version
 * @property {AttachmentType|any} attachmentType
 * @property {User|any} owner
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User|any} createdBy
 * @property {User|any} modifiedBy
 * @augments {DataObject}
 */
@EdmMapping.entityType('Attachment')
class Attachment extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    static convertFileName(file)
    {
        let fileName = file.name;
        if (typeof file.originalname === 'string') {
            fileName = file.originalname;
            // TODO: change this if busboy default encoding changes to utf-8 instead of latin1
            // buffer original name with latin1 encoding and back
            const buffer = Buffer.from(file.originalname, 'latin1');
            const testConversion = buffer.toString('latin1');
            // then check if the produced name is the same with originalName and parse it as utf-8
            if (testConversion == file.originalname) {
                fileName = Buffer.from(file.originalname, 'latin1').toString('utf-8');
            }
            fileName = fileName.replace(/\.[A-Z0-9]+$/ig, function (x) {
                return x.toLowerCase();
            });
        }
        return fileName;
    }
}
module.exports = Attachment;
