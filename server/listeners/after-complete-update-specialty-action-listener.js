import ActionStatusType from '../models/action-status-type-model';
import {DataError} from "@themost/common";

/**
 * @async
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    if (event.state !== 1) {
        return;
    }
    const target = event.model.convert(event.target);
    const context = event.model.context;
    // build description

    const student = await context.model('Student').where('id').equal(target.object).select('id',
        'studyProgram/name as studyProgram', 'studentIdentifier', 'studyProgramSpecialty/name as studyProgramSpecialty', 'studyProgramSpecialty/id as studyProgramSpecialtyId',
        'person/familyName as familyName', 'person/givenName as givenName').getItem();
    const newSpecialty = await context.model('StudyProgramSpecialty').where('id').equal(target.specialty).select('id',
        'studyProgram/name as studyProgram', 'name').getItem();
    if (student) {
        event.target.description = `${context.__('Change study program specialty')}: ${student.studentIdentifier} ${student.familyName} ${student.givenName} (${student.studyProgram}-${student.studyProgramSpecialty}) ->  (${newSpecialty.studyProgram}-${newSpecialty.name})`;

        if(student.studyProgramSpecialtyId === event.target.specialty) {
            throw new DataError('E_STUDENT_SPECIALTY_EXIST', context.__('E_STUDENT_SPECIALTY_EXIST'), null, 'UpdateSpecialtyAction');
        }
    }
}

/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state !== 2) {
        return;
    }
    const target = event.model.convert(event.target);
    const context = event.model.context;
    // get action status
    const actionStatus = await target.property('actionStatus').getItem();
    // if action status is other that active
    if (actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
        // exit
        return;
    }
    // get previous action status
    const previousActionStatus = event.previous && event.previous.actionStatus;
    if (previousActionStatus === null) {
        throw new Error('Previous status cannot be empty at this context.');
    }
    // validate previous status
    if (previousActionStatus.alternateName !== 'ActiveActionStatus') {
        // throw error for invalid previous action status
        throw new Error('Invalid action state. An action cannot be completed due to its previous state.');
    }
    // execute action by assigning student and specialty to an instance of StudentSpecialty class
    // get student
    const student = await target.property('object').silent().getItem();
    // get requested specialty
    const specialty = await target.property('specialty').silent().getItem();

    const studyProgramSpecialty = await context.model('StudyProgramSpecialty').where('id').equal(target.specialty).getItem();

    /**
     * get student specialty
     * @type {*}
     */
    const studentSpecialty = context.model('StudentSpecialty').convert({
        student: student,
        specialty: specialty
    });
    // do save
    await studentSpecialty.save();
    if (studentSpecialty.validationResult && studentSpecialty.validationResult.success){

        if (event.target.updateCourse || event.target.excludeCourse) {
            // get student courses and change properties from new program course
            /**
             * @type {StudentCourse[]}
             */
            const studentCourses = await context.model('StudentCourse').where('student').equal(student.id).silent().flatten().getAllItems();
            if (studentCourses.length) {
                let updateCourses = [];
                /**
                 * @type {SpecializationCourse[]}
                 */
                const programCourses = await context.model('SpecializationCourse').where('studyProgramCourse/studyProgram').equal(student.studyProgram)
                    .equal(student.studyProgram)
                    .and('specializationIndex').in(['-1', studyProgramSpecialty.specialty]).expand('studyProgramCourse')
                    .orderByDescending('specializationIndex')
                    .getAllItems();
                // change course properties
                studentCourses.forEach(studentCourse => {
                    const programCourse = programCourses.find(x => {
                        return x.studyProgramCourse && x.studyProgramCourse.course === studentCourse.course;
                    });
                    if (programCourse) {
                        if (event.target.updateCourse) {
                            // update student course attributes
                            // check if attributes are different
                            if (studentCourse.semester !== programCourse.semester ||
                                studentCourse.units !== programCourse.units ||
                                studentCourse.ects !== programCourse.ects ||
                                studentCourse.coefficient !== programCourse.coefficient ||
                                studentCourse.programGroup !== programCourse.studyProgramCourse.programGroup ||
                                studentCourse.groupPercent !== programCourse.studyProgramCourse.programGroupFactor ||
                                studentCourse.courseType !== programCourse.courseType ||
                                studentCourse.specialty !== programCourse.specializationIndex) {
                                studentCourse.semester = programCourse.semester;
                                studentCourse.units = programCourse.units;
                                studentCourse.ects = programCourse.ects;
                                studentCourse.coefficient = programCourse.coefficient;
                                studentCourse.programGroup = programCourse.studyProgramCourse.programGroup;
                                studentCourse.groupPercent = programCourse.studyProgramCourse.programGroupFactor;
                                studentCourse.courseType = programCourse.courseType;
                                studentCourse.specialty = programCourse.specializationIndex;
                                updateCourses.push(studentCourse);
                            }
                        }
                    } else {
                        // course not found
                        //update calculateGrade, calculateUnits
                        if (event.target.excludeCourse === true) {
                            studentCourse.calculateGrade = false;
                            studentCourse.calculateUnits = false;
                            updateCourses.push(studentCourse);
                        }
                    }
                });
                //save student courses
                await context.model('StudentCourse').save(updateCourses);
            }
        }
    }

    // return validation result
    return studentSpecialty.validationResult;
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    if (event.state !== 2) {
        return callback();
    }
    // execute async method
    return afterSaveAsync(event).then((validationResult) => {
        event.target.validationResult = validationResult;
        if (validationResult && !validationResult.success)
        {
            return callback(validationResult);
        }
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    // execute async method
    return  beforeSaveAsync(event).then((validationResult) => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
