import { DataObjectState } from '@themost/data';
import { DataConflictError } from '../errors';

const PREVIOUS_USER_ON_INSERT = 'UnknownPreviousUser';

/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
  // operate only if target has a user property
  if (!(event.target.hasOwnProperty('user') && event.target.user != null)) {
    return;
  }
  // get previous user state
  const previousUser =
    event.state === DataObjectState.Insert
      ? PREVIOUS_USER_ON_INSERT
      : event.previous && event.previous.user;
  // and target user state
  const targetUser = event.target.user.id || event.target.user;
  // if the user has not changed, exit
  if (previousUser === targetUser) {
    return;
  }
  const context = event.model.context;
  // first of all, always validate that the target user group is only Students
  const user = await context
    .model('User')
    .where('id')
    .equal(targetUser)
    .select('id')
    .expand('groups')
    .silent()
    .getItem();
  if (
    !(
      user &&
      Array.isArray(user.groups) &&
      // validate by name, since it is unique and non-editable
      user.groups.some((group) => group.name === 'Students')
    )
  ) {
    throw new DataConflictError(
      'A student can only be linked with a user that belongs to the Students group.'
    );
  }
  // then, check if that user is already linked to another student
  const userAlreadyTaken = await context
    .model('Student')
    .where('id')
    // safe to check on insert state also
    .notEqual(event.target.id)
    .and('user')
    .equal(user.id)
    .select('id')
    .silent()
    .count();
  // if user is free, exit
  if (!userAlreadyTaken) {
    return;
  }
  // try to reach the institute configuration
  const institute =
    event.state === DataObjectState.Insert
      ? await context
          .model('Department')
          .where('id')
          .equal(
            event.target.department &&
              (event.target.department.id || event.target.department)
          )
          .select('organization')
          .flatten()
          .silent()
          .value()
      : await event.model
          .convert(event.target)
          .property('department')
          .select('organization')
          .flatten()
          .silent()
          .value();
  // check the setting regarding student user sharing
  const allowStudentUserSharing = await context
    .model('InstituteConfiguration')
    .where('institute')
    .equal(institute)
    .select('allowStudentUserSharing')
    .silent()
    .value();
  // if the institute does not allow user sharing, throw error
  if (!allowStudentUserSharing) {
    throw new DataConflictError(
      context.__('The provided user already belongs to another student and the institute does not allow student user sharing.')
    );
  }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
  return beforeSaveAsync(event)
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}
