import {DataError} from "@themost/common";
class ScholarshipListener {

    /**
     * @param {DataEventArgs} event
     */
    static async beforeRemoveAsync(event) {
        const context = event.model.context;
        const target = event.target;
        const model = event.model;
        //scholarship can be deleted only if scholarship students don't exist
        const numberOfStudents = await context.model('StudentScholarship').where('scholarship').equal(target.id).count();
        if (numberOfStudents > 0) {
            //cannot delete scholarship
            throw new DataError('Cannot delete scholarship as there are students related to it.');
        }
        // remove scholarship rules
        // check if class has rules and remove them too.
        const rules = await model.convert(target).getScholarshipRules();
        if (rules && rules.length) {
            await context.model('Rule').silent().remove(rules);
        }
        // remove also scholarship validate actions
        const actions = await context.model('ScholarshipValidateAction').where('scholarship').equal(target.id).getAllItems();
        if (actions && actions.length) {
            await context.model('ScholarshipValidateAction').silent().remove(actions);
        }
    }
    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        const context = event.model.context;
        const target = event.model.convert(event.target);
        if (event.state === 1) {
            // check if scholarship has rules and save all
            if (target.rules && target.rules.length)
            {
                // add also scholarship rules
               // check if rules refer to other class
                const copyRules = target.rules.find(x=>{
                    return x.target !== target.id;
                });
                if (copyRules)
                {
                    //save rules
                    // check if scholarship has complex rules expression
                    const hasComplexRules = target.rules.find(x => {
                        return x.ruleExpression != null;
                    });
                    if (hasComplexRules) {
                        // ruleExpression should be replaced from new ids
                        let ruleExpression = target.rules[0].ruleExpression;
                        let newRules =[];
                        //each rule should be saved to get new id and replace it to ruleExpression
                        for (let i = 0; i < target.rules.length; i++) {
                            const rule = target.rules[i];
                            const oldId = rule.id;
                            delete rule.id;
                            // save new rule
                            const result = await target.setScholarshipRules([rule]);
                            const newRule = result[(result.length-1)];
                            newRules.push(newRule);
                            ruleExpression = ruleExpression.replace(new RegExp(`\\[%${oldId}\\]`, 'g'), `[%${newRule.id}]`);
                        }
                        // update ruleExpression
                        newRules = newRules.map(x => {
                            x.ruleExpression = ruleExpression;
                            return x;
                        });
                        // save rules
                        await target.setScholarshipRules(newRules);
                    } else {
                        // remove id and save
                        target.rules.map(x => {
                            delete x.id;
                        });
                        await target.setScholarshipRules(target.rules);
                    }
                }
            }
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return ScholarshipListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return ScholarshipListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}
