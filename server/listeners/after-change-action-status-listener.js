import { DataError } from '@themost/common';
import { DataObjectState } from '@themost/data';
import util from "util";

export function afterSave(event, callback) {
    (async () => {
        // on request action update
        const context = event.model.context;
        if (event.state === DataObjectState.Update) {
            const previous = event.previous;
            // get target object
            const target = await context.model(event.model.name).where('id').equal(event.target.id).silent().getTypedItem();
            if (previous == null) {
                throw new DataError('E_STATE', 'The previous state of the action cannot be determined', null, '')
            }
            // if there is a status change add to actionEventLog
            if (previous.actionStatus && previous.actionStatus.alternateName
                && target.actionStatus && target.actionStatus.alternateName !== previous.actionStatus.alternateName ) {

                // add to ActionEventLog
                const action = context.model('Action').convert(event.target);
                const eventLogTitle = util.format(context.translate("Change status from %s to %s"),
                    context.translate(`ActionStatus.${previous.actionStatus.name}`),
                    context.translate(`ActionStatus.${target.actionStatus.name}`));
                await action.addActionEventLog (eventLogTitle);
            }
        }
        if (event.state=== DataObjectState.Insert) {
            // add to ActionEventLog
            const action = context.model('Action').convert(event.target);
            const eventLogTitle = context.translate("Request action was created");
            await action.addActionEventLog(eventLogTitle);
        }
    })().then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
