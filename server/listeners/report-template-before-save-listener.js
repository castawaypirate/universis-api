import { DataEventArgs } from "@themost/data";
import { DataNotFoundError } from "@themost/common"; 


/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
async function beforeSaveAsync(event) {
    const { target, model, state = null } = event;

    if (state === 2) {
        const previous = await model
            .where('id').equal(target.id)
            .expand('exportFormats')
            .silent()
            .getItem();

        if (!previous) {
            throw new DataNotFoundError(model.context.__('The previous state of the report template cannot be determined.'));
        }

        if (
            previous.exportFormats 
            && previous.exportFormats.length 
            && event.target.hasOwnProperty('exportFormats')
            && Array.isArray(event.target.exportFormats)
        ) {
            const { exportFormats: prevFormats } = previous;
            const { exportFormats: targetFormats } = target;

            for (const prevFormat of prevFormats) {
                const targetFormat = targetFormats.find(format => format.id === prevFormat.id || format.alternateName === prevFormat.alternateName);
                if (!targetFormat) {
                    targetFormats.push({
                        '$state': 4,
                        ...prevFormat
                    })
                }
            }
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}