import util from 'util';
import {TraceUtils} from '@themost/common/utils';
import {ValidationResult} from "../errors";
import _ from 'lodash';
import {LangUtils} from '@themost/common/utils';
import { promisify } from 'util';
import { DataPermissionEventListener, PermissionMask, DataObjectState } from '@themost/data';
/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function beforeSave(event, callback) {
    (async () => {
        const context = event.model.context,
              target = event.model.convert(event.target),
              studentCourses = context.model('StudentCourse'),
              student = context.model('Student').convert(event.target.student);
        let validationResult;
        // on insert
        if (event.state === DataObjectState.Insert) {
            // get classStatus
            const classStatus = await target.property('courseClass')
                .select('status/alternateName')
                .silent()
                .value();
            // if class is closed
            if (classStatus === 'closed') {
                // first, validate current user against student
                const isMe = await student.is(':me');
                if (isMe) {
                    // and throw immediately
                    validationResult = Object.assign(new ValidationResult(false, 'FAIL', context.__('The course cannot be registered because the class is closed and does not accept registrations.')), {'statusCode': 403});
                    return validationResult;
                }
                // validate StudentCourseClass/BypassClosedStatus execute permission
                const bypassClosedStatus = {
                    model: event.model,
                    privilege: `${event.model.name}/BypassClosedStatus`,
                    mask: PermissionMask.Execute,
                    target: event.target,
                    throwError: false
                };
                const validator = new DataPermissionEventListener();
                const validateAsync = promisify(validator.validate)
                await validateAsync(bypassClosedStatus);
                // if the user does not have permission to bypass closed status
                if (bypassClosedStatus.result === false) {
                    // throw
                    validationResult = Object.assign(new ValidationResult(false, 'FAIL', context.__('The course cannot be registered because the class is closed and does not accept registrations.')), {'statusCode': 403});
                    return validationResult;
                }
            }
        }
        // update total class students
        await new Promise((resolve, reject) => {
            updateClassTotalStudents(context, target, event.state, ((err) => {
                if (err) {
                    return reject(err);
                }
                return resolve();
            }));
        });

        if ((event.state === DataObjectState.Insert || event.state === DataObjectState.Update) && target.courseClass) {
            const studentCourse = await studentCourses
                .where('course').equal(target['course'])
                .and('student').equal(target['student'])
                .silent().flatten().getItem();
            if (studentCourse) {
                // update student course attributes if this is the last registration
                if ((((target.registrationYear && target.registrationYear.id) > studentCourse.lastRegistrationYear)
                || ((target.registrationYear && target.registrationYear.id) === studentCourse.lastRegistrationYear
                    && target.registrationPeriod >= studentCourse.lastRegistrationPeriod))
                && studentCourse.isPassed === 0) {
                    // check studyProgram setting
                    const useCurrentCourseAttributes = await context.model('Student')
                        .where('id').equal(target['student'])
                        .select('studyProgram/useCurrentCourseAttributes as useCurrentCourseAttributes')
                        .silent()
                        .value();
                    if (LangUtils.parseInt(useCurrentCourseAttributes) !== 0) {
                        studentCourse.semester = target.hasOwnProperty('semester') && studentCourse.semester != (target.semester && target.semester.id) ? target.semester : studentCourse.semester;
                        studentCourse.units = target.hasOwnProperty('units') && studentCourse.units != target.units ? target.units : studentCourse.units;
                        studentCourse.coefficient = target.hasOwnProperty('coefficient') && studentCourse.coefficient != target.coefficient ? target.coefficient : studentCourse.coefficient;
                        studentCourse.courseType = target.hasOwnProperty('courseType') && studentCourse.courseType != (target.courseType && target.courseType.id) ? target.courseType : studentCourse.courseType;
                        studentCourse.programGroup = target.hasOwnProperty('programGroup') && studentCourse.programGroup != (target.programGroup && target.programGroup.id) ? target.programGroup : studentCourse.programGroup;
                        studentCourse.hours = target.hasOwnProperty('hours') && studentCourse.hours != target.hours ? target.hours : studentCourse.hours;
                        studentCourse.ects = target.hasOwnProperty('ects') && studentCourse.ects != target.ects ? target.ects : studentCourse.ects;
                        try {
                            // if the class title has changed
                            if (target.name !== studentCourse.courseTitle) {
                                // get default locale
                                const defaultLocale = context.getConfiguration().getSourceAt('settings/i18n/defaultLocale');
                                if (defaultLocale) {
                                    // fetch course class locales that are not in the default language
                                    const courseClassLocales = await context.model('CourseClassLocale')
                                        .where('object').equal(target.courseClass.toString())
                                        .and('inLanguage').notEqual(defaultLocale)
                                        .and('title').notEqual(null)
                                        .silent()
                                        .getAllItems();
                                    // if any exist
                                    if (Array.isArray(courseClassLocales) && courseClassLocales.length) {
                                        // transfer title to student course locale
                                        studentCourse.locales = courseClassLocales.map((locale) => {
                                            return {
                                                inLanguage: locale.inLanguage,
                                                courseTitle: locale.title
                                            };
                                        });
                                    }
                                }
                            }
                        } catch (err) {
                            // just log error
                            TraceUtils.error(`An error occured while trying to update StudentCourse locales for ${studentCourse.id}`);
                            TraceUtils.error(err);
                        }
                        // update also title
                        studentCourse.courseTitle = target.name || studentCourse.courseTitle;
                    }
                    // always update last registration year and period
                    studentCourse.lastRegistrationYear = target.registrationYear;
                    studentCourse.lastRegistrationPeriod = target.registrationPeriod;
                    // save silently
                    await studentCourses.silent().save(studentCourse);
                }
            } else {
                // add student course
                const item = {
                    course: target['course'],
                    student: target['student'],
                    courseTitle: target.name,
                    semester: target.semester,
                    specialty: target.specialty.specialty,
                    units: target.units,
                    coefficient: target.coefficient,
                    courseType: target.courseType,
                    programGroup: target.programGroup,
                    parentCourse: target.parentCourse,
                    hours: target.hours,
                    ects:target.ects,
                    lastRegistrationYear:target.registrationYear,
                    lastRegistrationPeriod:target.registrationPeriod
                };
                // save silently
                await studentCourses.silent().save(item);
            }
        }
    })().then((validationResult) => {
        if (validationResult && !validationResult.success) {
            return callback(validationResult);
        }
        return callback();
    }).catch((err) => {
        TraceUtils.error(err);
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterRemove(event, callback) {
    try {
        const context = event.model.context, target = event.target;
        updateClassTotalStudents(context, target, 4, function (err) {
            if (err) {
                TraceUtils.error(err);
                return callback();
            }
            else {
                callback();
            }
        });
    }
    catch (e) {
        callback(e)
    }
}


function updateClassTotalStudents(context,target, state,callback)
{
    try {
         if (state === 2)
            return callback(null);
        if (!target.courseClass)
            return callback(null);

        context.model('CourseClass').where('id').equal(target.courseClass).select(['id', 'numberOfStudents', 'maxNumberOfStudents'])
            .expand({
                'name': 'students',
                'options': {
                    '$select': 'courseClass,count(id) as total',
                    '$group': 'courseClass'
                }
            }).silent().first(function (err, result) {
            if (err) {
                return callback(err);
            }
            if (_.isNil(result))
                return callback(null);
            const maxNumberOfStudents = LangUtils.parseInt(result["maxNumberOfStudents"]);
            let numberOfStudents = LangUtils.parseInt(result["numberOfStudents"]);
            if (Array.isArray(result["students"])) {
                numberOfStudents = result["students"].length ? result["students"][0].total : numberOfStudents = 0;
            }

           // if (maxNumberOfStudents === 0)
           // return callback(null);
            if (state === 1)
                numberOfStudents += 1;

            if (numberOfStudents > maxNumberOfStudents && state !== 4 && maxNumberOfStudents!==0)
                return callback(new ValidationResult(false,'FAIL',util.format(context.__("Class limit (%s) has been reached."),maxNumberOfStudents)));

            if (numberOfStudents < 0) {
                numberOfStudents = 0;
            }
            context.unattended(function (cb) {
                context.model('CourseClass').save({id: target.courseClass, numberOfStudents: numberOfStudents}, function (err) {
                    cb(err);
                });
            }, function (err) {
                if (err) {
                    TraceUtils.error(err);
                }
                callback(err);
            });
        });
    }
    catch(e)
    {
        callback(e);
    }
}
