/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
import {DataError} from "@themost/common";
import ActionStatusType from '../models/action-status-type-model';
import Student from '../models/student-model';
import * as _ from 'lodash';

export function beforeSave(event, callback) {
    (async () => {
        // validate student state (on insert or update)
        // todo:: move this action to an upper level
        if (event.state === 1 || event.state === 2) {
            let context = event.model.context;
            // try to find the target action status (could be passed as id, alternateName etc)
            const actionStatus = await context.model('ActionStatusType').find(event.target.actionStatus).getItem();
            if (actionStatus == null) {
                throw new DataError('E_TARGET_STATUS', 'The target action status of the object cannot be found and cannot be empty.');
            }
            if (actionStatus.alternateName === ActionStatusType.CancelledActionStatus) {
                return;
            }
            /**
             * @type {Student|DataObject|*}
             */
            const student = context.model('Student').convert(event.target.object);
            const isActive = await student.silent().isActive();
            if (!isActive) {
                throw new DataError('ERR_STATUS',
                    'Invalid student status. Student removal action may execute upon an active student only.'
                    , null,
                    'Student', 'studentStatus');
            }
        }
    })().then( () => {
       return callback();
    }).catch( err => {
        return callback(err);
    });
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    (async () => {
        const context = event.model.context;
        let target;
        // get own properties
        const attributes = event.model.attributes.filter( attribute => {
            if (attribute.primary) {
                return false;
            }
            return attribute.model === event.model.name;
        }).map( attribute => {
            return attribute.name;
        });
        // on insert
        if (event.state === 1) {
            // get target object
            target = await context.model(event.model.name).where('id').equal(event.target.id).silent().getTypedItem();
            // validate completed state
            if (target.actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
                return;
            }

        }
        // on update
        else if (event.state === 2) {
            // get previous state
            const previousActionStatus = event.previous.actionStatus;
            // get target object
            target = await context.model(event.model.name).where('id').equal(event.target.id).silent().getTypedItem();
            if (((previousActionStatus.alternateName === ActionStatusType.ActiveActionStatus || previousActionStatus.alternateName === ActionStatusType.CancelledActionStatus) &&
                target.actionStatus.alternateName === ActionStatusType.CompletedActionStatus) === false) {
                return;
            }
        }
        else {
            return;
        }
        // do student removal (patch student)
        // pick student removal attributes
        const student = _.pick(target, attributes);
        // assign id and studentStatus
        Object.assign(student, {
            id: target.object.id,
            studentStatus: {
                alternateName: 'erased'
            }
        });
        // do update
        await context.model('Student').silent().save(student);

    })().then( () => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}
