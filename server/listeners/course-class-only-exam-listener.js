import { ApplicationServiceAsListener } from '../services/application-service-as-listener';
import { TraceUtils } from '@themost/common';

/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    try {
        if (event.state !== 1) {
            return;
        }
        const context = event.model.context,
            target = event.target;
        // required attributes
        if (!target.hasOwnProperty('student')
            || !target.hasOwnProperty('registrationYear')
            || !target.hasOwnProperty('registrationPeriod')
            || !target.hasOwnProperty('courseClass')) {
            return;
        }
        // check if courseClass has enabled absence limit
        const courseClass = context.model('CourseClass').convert(target.courseClass).getId();
        const courseClassItem = await context.model('CourseClass').where('id').equal(courseClass)
            .select('course', 'year', 'period', 'absenceLimit', 'department/organization/instituteConfiguration/absenceClassStatus as absenceClassStatus',
                'department/organization/instituteConfiguration/examOnlyClassStatus as examOnlyClassStatus')
            .flatten()
            .silent()
            .getItem();
        if (!courseClassItem) {
            return;
        }
        // only classes with absence limit should be checked
        const absenceLimit = courseClassItem.absenceLimit || 0;
        if (absenceLimit > 0) {
            // continue only if both of examOnlyClassStatus,absenceClassStatus have been set
            if (courseClassItem.examOnlyClassStatus != null && courseClassItem.absenceClassStatus != null) {
                // check status of the last course registration for this student
                // find last 2 course registrations for previous years or equal to course class year.
                // 2 records for the case of checking winter registration and summer registration exists also for same year:
                const student = context.model('Student').convert(target.student).getId();
                let studentCourseClasses = await context.model('StudentCourseClass').where('student').equal(student)
                    .and('courseClass/course').equal(courseClassItem.course)
                    .and('courseClass/year').lowerOrEqual(courseClassItem.year)
                    .orderByDescending('courseClass/year')
                    .thenByDescending('courseClass/period')
                    .select('id', 'courseClass/year as year', 'courseClass/period as period', 'courseClass/absenceLimit as absenceLimit', 'status')
                    .flatten()
                    .silent()
                    .take(2)
                    .getItems();
                // course registrations found
                if (studentCourseClasses && studentCourseClasses.length) {
                    // check also if year and period refers to previous course registration
                    const lastCourseRegistration = studentCourseClasses.find(x => {
                        return (courseClassItem.year === x.year && courseClassItem.period > x.period) || (courseClassItem.year > x.year);
                    });
                    // set status only if previous course registration is not equal to absenceClassStatus
                    if (lastCourseRegistration && lastCourseRegistration.status !== courseClassItem.absenceClassStatus) {
                        target.status = courseClassItem.examOnlyClassStatus;
                    }
                }
            }
        }
    }
    catch (e) {
       throw e;
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {

    beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        TraceUtils.error('CourseClassOnlyExamListener.beforeSave()');
        TraceUtils.error(err);
        return callback();
    });
}

export class CourseClassOnlyExam extends ApplicationServiceAsListener {
    constructor(app) {
        super(app);
        // install this listener as member of event listeners of StudentCourseClass model
        this.install('StudentCourseClass', __filename);
        (async function () {
            const context = app.createContext();
            context.finalize();
        })().then(() => {
            //
        }).catch(err => {
            TraceUtils.error(err);
        });
    }
}
