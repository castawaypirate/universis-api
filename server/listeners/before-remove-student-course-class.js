import { DataError } from '@themost/common';
import { ValidationResult } from '../errors';
import { promisify } from 'util';
import { DataPermissionEventListener, PermissionMask } from '@themost/data';

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterRemove(event, callback) {
	return StudentCourseClassListener.afterRemoveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

export function beforeRemove(event, callback) {
	return StudentCourseClassListener.beforeRemoveAsync(event)
		.then((validationResult) => {
			if (validationResult && !validationResult.success) {
				return callback(validationResult);
			} 
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

class StudentCourseClassListener {

	static async beforeRemoveAsync(event) {
		/**
		 * @type {DataContext|*}
		 */
		const context = event.model.context;
		const target = event.model.convert(event.target);
		if (event.previous == null) {
			throw new DataError('E_STATE', 'The previous state of the object cannot be determined', null, 'StudentCourseClass');
		}
		let validationResult;
		// get student
		const student = event.previous.student.id || event.previous.student;
		const convertedStudent = context.model('Student').convert(student);
		// check if student is active
		const isActive = await convertedStudent.silent().isActive();
		if (!isActive) {
			validationResult = Object.assign(new ValidationResult(false, 'DEL', context.__('Student is not active.')), {'statusCode': 403});
			// apply falsy validation result to target
			event.target.validationResult = validationResult;
			return validationResult;
		}
		// check if registration is closed
		const registrationStatus = await target.property('registration').select('status/alternateName').silent().value();
		if (registrationStatus === 'closed') {
			validationResult = Object.assign(new ValidationResult(false, 'DEL', context.__('Student period registration is closed and cannot be modified.')), {'statusCode': 403});
			// apply falsy validation result to target
			event.target.validationResult = validationResult;
			return validationResult;
		}
		// check class status
		const classStatus = await target.property('courseClass').select('status/alternateName').silent().value();
		// if class is closed
		if (classStatus === 'closed') {
			// first, validate current user against student
			const isMe = await convertedStudent.is(':me');
			if (isMe) {
				// immediately exit
				validationResult = Object.assign(new ValidationResult(false, 'DEL', context.__('The course cannot be deleted because the class is closed.')), {'statusCode': 403});
				// apply falsy validation result to target
				event.target.validationResult = validationResult;
				return validationResult;
			}
			// before blocking the removal, validate StudentCourseClass/BypassClosedStatus execute permission
			const bypassClosedStatus = {
				model: event.model,
				privilege: `${event.model.name}/BypassClosedStatus`,
				mask: PermissionMask.Execute,
				target: event.target,
				throwError: false
			};
			const validator = new DataPermissionEventListener();
			const validateAsync = promisify(validator.validate)
			await validateAsync(bypassClosedStatus);
			// if the user does not have permission to bypass closed status
			if (bypassClosedStatus.result === false) {
				validationResult = Object.assign(new ValidationResult(false, 'DEL', context.__('The course cannot be deleted because the class is closed.')), {'statusCode': 403});
				// apply falsy validation result to target
				event.target.validationResult = validationResult;
				return validationResult;
			}
		}
		// check if student has absences
		const absences = await event.model.where('id').equal(event.previous.id)
			.select('absences').silent().value();
		if (absences > 0) {
			validationResult = Object.assign(new ValidationResult(false, 'DEL', context.__('The course cannot be deleted because of class absences for the student.')), {'statusCode': 403});
			// apply falsy validation result to target
			event.target.validationResult = validationResult;
			return validationResult;
		}
		// get courseClass
		const courseClass = event.previous.courseClass.id || event.previous.courseClass;
		// get student grades for specific course class
		const studentGrades = await context.model('StudentGrade')
			.where('student').equal(student)
			.and('courseClass').equal(courseClass)
			.select('id')
			.silent()
			.count()
		// if there are student grades for specific course class
		if (studentGrades > 0) {
			validationResult = Object.assign(new ValidationResult(false, 'DEL', context.__('The course cannot be deleted because it has been graded')), {'statusCode': 403});
			if (event.target) {
				// apply falsy validation result to target
				event.target.validationResult = validationResult;
			}
			return validationResult;
		}
	}

	static async afterRemoveAsync(event) {
		/**
		 * @type {DataContext|*}
		 */
		const context = event.model.context;
		if (event.previous == null) {
			throw new DataError('E_STATE', 'The previous state of the object cannot be determined', null, 'StudentCourseClass');
		}
		// get student
		const student = event.previous.student.id || event.previous.student;
		// get courseClass
		const courseClass = event.previous.courseClass.id || event.previous.courseClass;
		const courseClassItem = await context.model('CourseClass')
			.where('id').equal(courseClass)
			.select('course', 'department/organization/instituteConfiguration/allowRemoveStudentCourse as isAvailable')
			.expand('course')
			.silent()
			.getItem();
		// get course
		const course = courseClassItem.course;
		// get student last registration for this course and update student course year and period
		const lastRegistration = await context.model('StudentCourseClass')
			.where('student').equal(student)
			.and('courseClass/course').equal(course.id)
			.select('id','registration/registrationYear as registrationYear','registration/registrationPeriod as registrationPeriod')
			.orderByDescending('registration/registrationYear')
			.thenByDescending('registration/registrationPeriod')
			.silent()
			.getItem();
		// if last registration exists update year and period and return
		if (lastRegistration) {
			try {
				// update student course lastRegistrationYear and period
				let studentCourse = await context.model('StudentCourse')
					.where('student').equal(student)
					.and('course').equal(course.id)
					.select('id')
					.silent()
					.getItem();

				studentCourse.lastRegistrationYear = lastRegistration.registrationYear;
				studentCourse.lastRegistrationPeriod = lastRegistration.registrationPeriod;
				await context.model('StudentCourse').silent().save(studentCourse);
				return;
			} catch (err) {
				// do nothing
				return;
			}
		}
		if (!(courseClassItem.isAvailable && courseClassItem.isAvailable === true)) {
			return;
		}
		// for a simple course
		if (course.courseStructureType && course.courseStructureType === 1) {
			// get studentCourse
			const removeCourse = await context.model('StudentCourse')
				.where('student').equal(student)
				.and('course').equal(course.id)
				.silent()
				.getItem();
			// check for exemption
			if (removeCourse.registrationType != null && removeCourse.registrationType !== 1) {
				try {
					await context.model('StudentCourse').silent().remove(removeCourse);
				} catch (err) {
					//
					return;
				}
			}
		} else if (course.courseStructureType && course.courseStructureType === 8 /* course part */) {
			// get parent course
			const parentCourse = course.parentCourse;
			// get all relevant course parts
			const courseParts = await context.model('Course').where('parentCourse').equal(parentCourse)
				.and('id').notEqual(course.id)	
				.select('id')
				.silent().getItems();
			const values = courseParts.map(course => course.id);
			// check if any of them has been registered
			const isRegistered = await context.model('StudentCourseClass')
				.where('student').equal(student)
				.and('courseClass/course').in(values)
				.silent()
				.count();
			if (isRegistered) {
				return;
			}
			// add parent course and current course to check for exemption
			values.push(parentCourse);
			values.push(course.id);
			const hasExemptions = await context.model('StudentCourse')
				.where('student').equal(student)
				.and('course').in(values)
				.and('registrationType').equal(1)
				.silent().count();
			if (hasExemptions) {
				return;
			}
			// finally, remove parent course
			const removeCourse = await context.model('StudentCourse')
				.where('student').equal(student)
				.and('course').equal(parentCourse)
				.silent()
				.getItem();
			try {
				// remove studentCourse
				await context.model('StudentCourse').silent().remove(removeCourse);
			} catch (err) {
				// do nothing
				return;
			}
		}
	}
}
