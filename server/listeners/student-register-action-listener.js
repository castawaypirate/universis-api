import {DataError, DataNotFoundError, HttpForbiddenError, HttpServerError} from "@themost/common/errors";
import {TraceUtils} from '@themost/common/utils';
import {ValidationResult} from "../errors";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    try {
        callback();
    }
    catch (e) {
        callback(e);
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    try {
        /**
         * get target object
         * @type {StudentRegisterAction|*}
         */
        const target = event.target;
        /**
         * get current context
         * @type {DataContext}
         */
        const context = event.model.context;
        // if event state is other than insert
        if (event.state !== 1) {
            // exit
            return callback();
        }
        // get action registration period
        const registrationPeriod = context.model('AcademicPeriod').convert(target.registrationPeriod).getId();
        // get action registration year
        const registrationYear = context.model('AcademicYear').convert(target.registrationYear).getId();
        // get action student
        const student = context.model('Student').convert(target.object).getId();
        // validate registration period info
        return context.model('Student')
            .where('id').equal(student)
            .select('department').expand('department')
            .silent()
            .value().then(department => {
                if (department == null) {
                    return callback(new DataError('EDEPT', 'Student department cannot be found.'));
                }
                return context.model('Institute').where('id').equal(department.organization)
                    .expand('instituteConfiguration', 'registrationPeriods')
                    .silent()
                    .getItem().then(institute => {
                        if (institute == null) {
                            return callback(new DataError('EINST', 'Student institute cannot be found.'));
                        }
                        if (institute.instituteConfiguration == null) {
                            return callback(new ValidationResult(false, 'ECONF', context.__('Institute configuration cannot be found or is inaccessible.')));
                        }
                        if (institute.instituteConfiguration && !institute.instituteConfiguration.useStudentRegisterAction) {
                            return callback(new DataError('ECONF', 'Student register action is not allowed.'));
                        }
                        // check current registration period info
                        const registrationPeriodInfo = institute.registrationPeriods.find(registrationPeriodInfo => {
                            return registrationPeriodInfo.academicYear === registrationYear &&
                                registrationPeriodInfo.academicPeriod === registrationPeriod
                        });
                        if (registrationPeriodInfo == null) {
                            return callback(new ValidationResult(false, 'EPERIOD', context.__('Institute registration period info cannot be found.'), null));
                        }
                        // get current date
                        const currentDate = new Date();
                        currentDate.setHours(0,0,0,0);
                        registrationPeriodInfo.registrationPeriodStart.setHours(0,0,0,0);
                        registrationPeriodInfo.registrationPeriodEnd.setHours(0,0,0,0);
                        const validDate = registrationPeriodInfo.registrationPeriodStart <= currentDate && registrationPeriodInfo.registrationPeriodEnd >= currentDate;
                        if (!validDate) {
                            return callback(new ValidationResult(false, 'EVAL', context.__('Institute registration period is closed or it has not been opened yet.')));
                        }
                        return callback();
                    });
            }).catch(err => {
                return callback(err);
            });
    }    catch (err) {
        callback(err)
    }
}
