import {DataError, DataNotFoundError} from "@themost/common";
import {DataModel} from "@themost/data";
class CourseClassListener {

    /**
     * @param {DataEventArgs} event
     */
    static async beforeSaveAsync(event) {
        const context = event.model.context;
        const target = event.target;
        // exit on update
        if (event.state !== 1) {
            return;
        }
        // check if course is complex and throw error
        const course = await context.model('Course').where('id').equal(target.course).expand('locales').getItem();
        if (!course) {
            // course does not exist
            throw new DataNotFoundError(context.__('Course not found'));
        }
        if (course.courseStructureType === 4) {
            // creating courseClass for complex course is not allowed
            throw new DataError(context.__('Creating course class for complex course is not allowed'));
        }
        // add also statistic (supplied by copy action)
        if (event.target.statistic && event.target.statistic.courseClass) {
            event.target.statistic = {
                "hasRules": (target.rules && target.rules.length) || 0,
                "registrationType": event.target.statistic.registrationType,
            }
        } else {
            // initialize statistic for new class
            event.target.statistic = {
                "hasRules": (target.rules && target.rules.length) || 0,
            }
        }
        if (Array.isArray(event.target.locales) && event.target.locales.length &&
            Array.isArray(course.locales) && course.locales.length) {
            // get default locale
            const defaultLocale = context.getConfiguration().getSourceAt('settings/i18n/defaultLocale');
            if (defaultLocale) {
                // exclude course class locale of the default system lang
                event.target.locales.filter((locale) => {
                    return locale.inLanguage !== defaultLocale;
                }).forEach((courseClassLocale) => {
                    // if the title is not set
                    if (!courseClassLocale.title) {
                        // find and assign the course's title
                        const courseLocale = course.locales.find((courseLocaleItem) => {
                            return courseLocaleItem.inLanguage === courseClassLocale.inLanguage;
                        });
                        if (courseLocale && courseLocale.name) {
                            courseClassLocale.title = courseLocale.name;
                        }
                    }
                });
            }
        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async beforeRemoveAsync(event) {
        const context = event.model.context;
        const target = event.target;
        const model=event.model;
        const getReferenceMappings = DataModel.prototype.getReferenceMappings;
        model.getReferenceMappings   = async function () {
            const res = await getReferenceMappings.bind(this)();
            // remove readonly model CourseExamStudentGrade, StudentAvailableClasses from mapping before delete
            const mappings = ['CourseExamStudentGrade','StudentAvailableClass'];
            return res.filter((mapping) => {
                return mappings.indexOf(mapping.childModel)<0;
            });
        };
            //check courseClass connections
            //course class can be deleted only if courseClassStudents don't exist
            const numberOfStudents = await context.model('StudentCourseClass').where('courseClass').equal(target.id).count();
            if (numberOfStudents === 0) {
                const exams = await context.model('CourseExamClass').where('courseClass').equal(target.id).select('id').getItems();
                if (exams && exams.length) {
                    await context.model('CourseExamClass').remove(exams);
                }
            } else {
                //cannot delete courseClass
                throw new DataError('Cannot delete course class');
            }
            // remove courseClass rules
            // check if class has rules and remove them too.
            const rules = await model.convert(target).getClassRegistrationRules();
            if (rules && rules.length) {
                await context.model('Rule').silent().remove(rules);
            }

    }
    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        const context = event.model.context;
        const target = event.model.convert(event.target);
        if (event.state === 1) {
            // check if courseClass has sections, instructors and rules and save all
            // save course class instructors
            if (target.instructors && target.instructors.length) {
                target.instructors = target.instructors.map(x => {
                    x.courseClass = target.id;
                    return x;
                });
                await context.model('CourseClassInstructor').save(target.instructors);
            }
            // save course class sections
            if (target.sections && target.sections.length) {
                target.sections = target.sections.map(x => {
                    x.courseClass = target.id;
                    return x;
                });
                await context.model('CourseClassSection').save(target.sections);
            }
            if (target.rules && target.rules.length)
            {
                // add also courseClass rules
               // check if rules refer to other class
                const copyRules = target.rules.find(x=>{
                    return x.target !== target.id;
                });
                if (copyRules)
                {
                    //save rules
                    // check if courseClass has complex rules expression
                    const hasComplexRules = target.rules.find(x => {
                        return x.ruleExpression != null;
                    });
                    if (hasComplexRules) {
                        // ruleExpression should be replaced from new ids
                        let ruleExpression = target.rules[0].ruleExpression;
                        let newRules =[];
                        //each rule should be saved to get new id and replace it to ruleExpression
                        for (let i = 0; i < target.rules.length; i++) {
                            const rule = target.rules[i];
                            const oldId = rule.id;
                            delete rule.id;
                            // save new rule
                            const result = await target.setClassRegistrationRules([rule]);
                            const newRule = result[(result.length-1)];
                            newRules.push(newRule);
                            ruleExpression = ruleExpression.replace(new RegExp(`\\[%${oldId}\\]`, 'g'), `[%${newRule.id}]`);
                        }
                        // update ruleExpression
                        newRules = newRules.map(x => {
                            x.ruleExpression = ruleExpression;
                            return x;
                        });
                        // save rules
                        await target.setClassRegistrationRules(newRules);
                    } else {
                        // remove id and save
                        target.rules.map(x => {
                            delete x.id;
                        });
                        await target.setClassRegistrationRules(target.rules);
                    }
                }
            }
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return CourseClassListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return CourseClassListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return CourseClassListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}
