import { DataNotFoundError } from '@themost/common';
import { DataConflictError } from '../errors';
import util from 'util';


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
	return beforeRemoveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterRemove(event, callback) {
	return afterRemoveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

/**
 * @param {DataEventArgs} event
 */
export async function beforeRemoveAsync(event) {
	/**
	 * A student period registration can be deleted only if
	 * - The student is active
	 * - No classes are registered
	 * - The registration has not been edited at any time by the student
	 */

	const context = event.model.context;

	// get self and validate
	const studentPeriodRegistration = await context
		.model('StudentPeriodRegistration')
		.where('id')
		.equal(event.target.id)
		.select('id', 'student/studentStatus/alternateName as studentStatus')
		.expand('classes', 'documents')
        .silent()
		.getItem();
	if (studentPeriodRegistration == null) {
		throw new DataNotFoundError(
			'The specified student period registration cannot be found.'
		);
	}
	// validate students status
	if (studentPeriodRegistration.studentStatus !== 'active') {
		throw new DataConflictError(
			context.__(
				'The student period registration cannot be deleted because the student is not active.'
			)
		);
	}
	// validate registered classes
	// IMPORTANT NOTE: This validation happens by the internal @themost listener (e.g referenced by another entity)
	// This only produces a more specific error message
	if (
		studentPeriodRegistration.classes &&
		Array.isArray(studentPeriodRegistration.classes) &&
		studentPeriodRegistration.classes.length
	) {
		throw new DataConflictError(
			context.__(
				'The student period registration cannot be deleted because it has at least one registered class.'
			)
		);
	}
	// validate documents
	// IMPORTANT NOTE: This validation happens by the internal @themost listener (e.g referenced by another entity)
	// This only produces a more specific error message
	// TODO: Revisit this line of code, if and when documents are replaced by @universis/changelog.
	if (
		studentPeriodRegistration.documents &&
		Array.isArray(studentPeriodRegistration.documents) &&
		studentPeriodRegistration.documents.length
	) {
		throw new DataConflictError(
			context.__(
				'The student period registration cannot be deleted because it has been created or edited by the student.'
			)
		);
	}
	// exit
	return;
}

export async function afterRemoveAsync(event) {

	const context = event.model.context;

	// get student info
	const information = await context.model('Student')
		.where('id').equal(event.previous.student)
		.select('department',
				'studentIdentifier',
				'person/familyName as familyName',
				'person/givenName as givenName')
		.flatten()
		.silent()
		.getItem();
	if (information == null) {
		return;
	}
	// get registrationYear and period (must always exist in previous state)
	const registrationYear = event.previous.registrationYear.id || event.previous.registrationYear;
	const registrationPeriod = event.previous.registrationPeriod.id || event.previous.registationPeriod;
	const eventTitle = util.format('%s [%s] %s %s %s-%s (%s)',
		context.__('Registration deleted'), information.studentIdentifier,
		information.familyName, information.givenName,
		registrationYear, registrationYear + 1, registrationPeriod);

	// create object event log for deletion
	const objectEventLog = {
		title: eventTitle,
		eventType: 3,
		username: context.user && context.user.name,
		object: event.previous.id,
		objectType: event.model.name,
		eventSource: event.model.name,
		department: information.department
	};

	await context.model('ObjectEventLog')
		.silent()
		.save(objectEventLog);
}
