import {ProgramCourseNotFound} from "../errors";
import _ from 'lodash';
/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function beforeSave(event, callback) {
    try {
        if (event.state !== 1) return callback();
        //get student course data (if any)
        const context = event.model.context, target = event.target, students = context.model('Student'), studentCourses = context.model('StudentCourse'), programCourses = context.model('ProgramCourse'), courseClasses = context.model('CourseClass');
        //get course class associated with this item
        const courseClass = courseClasses.convert(target.courseClass);
        courseClass.courseOf(function(err, course) {
           if (err) { return callback(err); }
            //get student associated with this item
            const student = students.convert(target.student);
            studentCourses.where('course').equal(course).and('student').equal(student.getId()).flatten().silent().select('CourseAttributes').first(function(err, studentCourse) {
                if (err) { return callback(err); }
                //course attributes was found in student course
                if (studentCourse) {
                    Object.assign(target, studentCourse);
                }
                else {
                    student.programOf(function(err, program) {
                        if (err) { return callback(err); }
                        programCourses.where('id').equal(program).and('course').equal(course).select('CourseAttributes').first(function(err, programCourse) {
                            if (err) { return callback(err); }
                            if (_.isNil(programCourse)) {
                                return callback(new ProgramCourseNotFound('The specified course cannot be found in program courses.'));
                            }
                            else {
                                Object.assign(target, programCourse);
                                callback();
                            }
                        });
                    });
                }
            });
        });
    }
    catch (e) {
        callback(e)
    }
}
