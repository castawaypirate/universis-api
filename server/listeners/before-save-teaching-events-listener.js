import { DataError } from '@themost/common';

/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    if (event.state === 1) {
        const { model: { context }, target: { eventHoursSpecification } } = event;

        if (eventHoursSpecification) {
            if (typeof eventHoursSpecification !== 'object' 
                && eventHoursSpecification.validFrom && eventHoursSpecification.validThrough
                && eventHoursSpecification.opens && eventHoursSpecification.closes && eventHoursSpecification.duration) {
                throw new DataError('ERR_INVALID_DATA', 'EventHoursSpecification must be of type Object with the appropriate properties (validFrom, validThrough, etc.)', null, 'TeachingEvent');
            }
            try {
                event.target.eventHoursSpecification = (await context.model('EventHoursSpecification').save(eventHoursSpecification)).id;
            } catch (e) {
                return new Error('Could not save event hours specification ')
            }
        }

        try {
            // get timetable (the method will not return timetable for sub-events)
            const timetable = await context.model('EducationEvent').convert(event.target).getTimetable();
            if (timetable) {
                event.target.superEvent = timetable.id;
            }
        } catch (error) {
            // if the method fails but the event's parent is a timetable, validate it's time-frame to provide a more specific error 
            if (event.target.superEvent) {
                const timetable = await context.model('TimetableEvent').where('id').equal(event.target.superEvent).silent().getItem();

                if (timetable) {
                    const startDate = new Date(eventHoursSpecification ? eventHoursSpecification.validFrom : event.target.startDate);
                    const endDate = new Date(eventHoursSpecification ? eventHoursSpecification.validThrough : event.target.endDate);
                    const timetableStartDate = new Date(timetable.startDate);
                    const timetableEndDate = new Date(timetable.endDate);
                    const locale = context.locale;
                    if ((startDate.getTime() - timetableStartDate.getTime() < 0) || (new Date(endDate).setHours(0,0,0,0) - timetableEndDate.getTime() > 0)) {
                        throw new Error(`Event out of bounds. The event must be in the available time-frame (${timetableStartDate.toLocaleDateString(locale)} - ${timetableEndDate.toLocaleDateString(locale)})`)
                    }
                }
            } else {
                throw error
            }
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
