import { DataError, HttpForbiddenError } from '@themost/common';
import { DataObjectState } from '@themost/data';

export function beforeSave(event, callback) {
  (async () => {
    const context = event.model.context;
    let account;
    // on insert
    if (event.state === DataObjectState.Insert) {
      // get account from target state (non-nullable, non-editable)
      account = event.target.account.id || event.target.account;
    } else {
      // on update, get account from previous state
      if (event.previous == null) {
        throw new DataError(
          'E_PREVIOUS',
          'The previous state of the object cannot be determined.',
          null,
          'Permission'
        );
      }
      account = event.previous.account;
    }
    // validate limited access
    await validateLimitedAccess(context, account);
  })()
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}

export function beforeRemove(event, callback) {
  (async () => {
    const context = event.model.context;
    if (event.previous == null) {
      throw new DataError(
        'E_PREVIOUS',
        'The previous state of the object cannot be determined.',
        null,
        'Permission'
      );
    }
    // get account from previous state
    const account = event.previous.account;
    // validate limited access
    await validateLimitedAccess(context, account);
  })()
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}

async function validateLimitedAccess(context, account) {
  if (context == null || account == null) {
    return;
  }
  // get account additional type
  const accountType = await context
    .model('Account')
    .where('id')
    .equal(account)
    .select('additionalType')
    .silent()
    .value();
  // if it is not a group, exit
  if (accountType !== 'Group') {
    return;
  }
  // get groupAttributes of group
  const group = await context
    .model('Group')
    .where('id')
    .equal(account)
    .expand('groupAttributes')
    .select('id')
    .silent()
    .getItem();
  // if the group is flagged with limited access
  if (
    group &&
    Array.isArray(group.groupAttributes) &&
    group.groupAttributes.find((attribute) => attribute === 'limitedAccess')
  ) {
    // throw forbidden error
    throw new HttpForbiddenError(
      context.__(
        'The group privileges cannot be modified as it has been marked as a limited access core group.'
      )
    );
  }
}
