import ActionStatusType from '../models/action-status-type-model';
import {GenericDataConflictError} from "../errors";
import {DataNotFoundError} from "@themost/common";

/**
 * @async
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    if (event.state !== 1) {
        return;
    }
    const target = event.model.convert(event.target);
    const context = event.model.context;
    target.student = typeof event.target.student === "object"? event.target.student.id: event.target.student;
    target.courseExam = typeof event.target.courseExam === "object"? event.target.courseExam.id: event.target.courseExam;
    // check if student is active, course is passed and grade status is normal
    let studentGrade = await context.model('StudentGrade').where('student').equal(target.student)
        .and('courseExam').equal(target.courseExam)
        .and('student/studentStatus/alternateName').equal('active')
        .and('status/alternateName').equal('normal')
        .select('id','student','courseExam','student/studentIdentifier as studentIdentifier','student/person/familyName as familyName',
            'student/person/givenName as givenName','student/studyProgram/maxNumberOfRemarking as maxNumberOfRemarking','courseExam/description as courseExamDescription', 'formattedGrade', 'isPassed')
        .silent()
        .getItem();

    // get student
    const student = await target.property('student').silent().getTypedItem();
    const isMe = await student.is(':me');
    if (isMe && studentGrade) {
        // student can only request remark for passed grades
        if (studentGrade.isPassed === 0) {
            studentGrade = null;
        }
    }
    if (!studentGrade) {
        // throw error
        throw new GenericDataConflictError('EFAIL', context.__('Student grade remark action can be applied only on passed courses of active students'));
    }
    // check if max number of allowed remarks has been reached
    const remarks = await context.model('StudentGradeRemarkAction').where('student').equal(target.student)
        .and('actionStatus/alternateName').notEqual(ActionStatusType.CancelledActionStatus).count();
    if (studentGrade.student && studentGrade.maxNumberOfRemarking != null
        && studentGrade.maxNumberOfRemarking > 0) {
        if (remarks >= studentGrade.maxNumberOfRemarking) {
            throw new GenericDataConflictError('E_MAX_REACHED', context.__('Study program\'s max number of allowed remarks has been reached'));
        }
    }
    // set studentGrade, name and description fields
    event.target.studentGrade = studentGrade.id;
    event.target.formattedGrade = studentGrade.formattedGrade;
    event.target.name = event.target.name || context.__('Student grade remark request action');
    event.target.description = `${context.__('Student grade remark request action')}: ${studentGrade.studentIdentifier} ${studentGrade.familyName} ${studentGrade.givenName} (${studentGrade.courseExamDescription}) `;
}

/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {

    const context = event.model.context;
    const target = event.model.convert(event.target);
    const actionStatus = await target.property('actionStatus').getItem();
    const previousActionStatus = event.previous && event.previous.actionStatus;

    if ((event.state === 1 && actionStatus.alternateName === ActionStatusType.CompletedActionStatus) ||
        (event.state === 2 && previousActionStatus.alternateName != actionStatus.alternateName )) {

        if (event.state === 2 && previousActionStatus === null) {
            throw new Error('Previous status cannot be empty at this context.');
        }
        // load request
        const request = await context.model(event.model.name).where('id').equal(target.id).flatten().getItem();
        // get studentGrade and check if student is active
        const studentGrade = await context.model('StudentGrade').where('id').equal(request.studentGrade)
            .and('courseExam').equal(request.courseExam)
            .and('student/studentStatus/alternateName').equal('active')
            .expand('status')
            .getItem();
        if (!studentGrade) {
            throw new DataNotFoundError('The specified student grade cannot be found or is inaccessible');
        }
        // if request is new and status is completed or update request and status is completed, set grade as remark
        if ((previousActionStatus && previousActionStatus.alternateName !== ActionStatusType.CompletedActionStatus && actionStatus.alternateName === ActionStatusType.CompletedActionStatus)
            || (event.state === 1 && actionStatus.alternateName === ActionStatusType.CompletedActionStatus)
        ) {
            // load and update studentGrade set status to remark
            // get student
            const student = await target.property('student').silent().getTypedItem();
            const isMe = await student.is(':me');
            if (studentGrade.status.alternateName === 'normal') {
                studentGrade.grade2 = studentGrade.grade1 || studentGrade.examGrade;
                studentGrade.examGrade = null;
                studentGrade.grade1 = null;
                studentGrade.status = {'alternateName': "remark"};
                // save studentGrade
                if (isMe && !studentGrade.isPassed === 1) {
                    throw new GenericDataConflictError('E_INVALID', context.__('Student grade is not valid for remark'));
                }
                await context.model('StudentGrade').save(studentGrade);
            } else {
                throw new GenericDataConflictError('E_INVALID', context.__('Student grade is not valid for remark'));
            }
        }
        if (event.state === 2 && actionStatus.alternateName === ActionStatusType.CancelledActionStatus) {
            // check if remark exists
            if (studentGrade.status.alternateName === 'remark') {
                // check if passed grade exist for student and course
                const passedCourse = await context.model('StudentGrade').where('student').equal(target.student)
                    .and('courseExam/course').equal(studentGrade.courseExam.course)
                    .and('isPassed').equal(1).count();
                if (passedCourse) {
                    throw new GenericDataConflictError('E_PASSED', context.__('Cannot revert remark. Student course is already passed.'));
                }
                // revert grade
                studentGrade.grade1 = studentGrade.grade2;
                studentGrade.examGrade = studentGrade.grade2;
                studentGrade.grade2 = null;
                studentGrade.status = {'alternateName': "normal"};
                // save studentGrade
                await context.model('StudentGrade').save(studentGrade);
            }
        }
    } else {
        return;
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    // execute async method
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    // execute async method
    return  beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
