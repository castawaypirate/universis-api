import { DataError } from '@themost/common';
import { DataObjectState } from '@themost/data';
import ActionStatusType from "../models/action-status-type-model";

export function beforeSave(event, callback) {
    (async () => {
        // on request action update
        if (event.state === DataObjectState.Update) {
            const context = event.model.context;
            const previous = event.previous;
            // get target object
            const target = event.target;
            if (previous == null) {
                throw new DataError('E_STATE', 'The previous state of the action cannot be determined', null, '')
            }
            if (target.hasOwnProperty('actionStatus') && target.actionStatus!=null) {
                // try to find the target action status
                const actionStatus = await context.model('ActionStatusType').find(event.target.actionStatus).getItem();
                if (actionStatus == null) {
                    throw new DataError('E_TARGET_STATUS', 'The target action status of the object cannot be found and cannot be empty.');
                }

                // check if action status can change
                if (previous.actionStatus && previous.actionStatus.alternateName
                    && actionStatus.alternateName !== previous.actionStatus.alternateName) {
                    // check if action status can be active
                    if (actionStatus.alternateName === ActionStatusType.ActiveActionStatus) {
                        if (previous.actionStatus.alternateName === ActionStatusType.CompletedActionStatus) {
                            // check if document action is related to documentNumberSeriesItem
                            const request = await context.model(event.model.name).where('id').equal(target.id).expand('result').getItem();
                            if (request.result) {
                                // check if document is published
                                if (request.result.published) {
                                    throw new DataError('E_DOCUMENT_STATUS', 'The document request action is related to a published document.');
                                }
                                else
                                {
                                    // try to cancel document item
                                    request.result.owner = null;
                                    request.result.documentStatus = {alternateName:'CancelledDocumentStatus'};
                                    await context.model('DocumentNumberSeriesItem').silent().save(request.result);
                                }
                            }
                        }
                    }

                }
            }
        }
    })().then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
