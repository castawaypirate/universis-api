import { DataError, HttpForbiddenError } from '@themost/common';
import { DataObjectState } from '@themost/data';
import ActionStatusType from '../models/action-status-type-model';

export function beforeSave(event, callback) {
    (async () => {
        const context = event.model.context;
        if (event.state === DataObjectState.Insert) {
            const student = context.model('Student').convert(event.target.student);
            if (student == null) {
                throw new DataError('E_STUDENT', 'The student cannot be empty at this context.');
            }
            // block any request creation that is not from the student
            // this ensures that the preferredSpecialty student endpoint cannot be bypassed
            const isMe = await student.is(':me');
            if (!isMe) {
                throw new HttpForbiddenError();
            }
            const specialtyName = await context.model('StudyProgramSpecialty')
                .where('id').equal(event.target.specialty.id || event.target.specialty)
                .select('name')
                .silent()
                .value();
            // set name and description
            event.target.name = (event.target.name || context.__('Preferred specialty request')) + ` (${specialtyName})`;
            event.target.description = event.target.description || context.__('Preferred specialty request');
        }
        if (event.state === DataObjectState.Update && event.target.hasOwnProperty('actionStatus')) {
            // get previous action status
            const previousActionStatus = event.previous && event.previous.actionStatus && event.previous.actionStatus.alternateName;
            // and validate it
            if (previousActionStatus == null) {
                throw new DataError('E_PREVIOUS_STATUS', 'The previous action status of the object cannot be determined.');
            }
            // exit if the previous status is active
            if (previousActionStatus === ActionStatusType.ActiveActionStatus) {
                return;
            }
            const targetActionStatus = await context.model('ActionStatusType').find(event.target.actionStatus).getItem();
            // and validate it
            if (targetActionStatus == null) {
                throw new DataError('E_TARGET_STATUS', 'The target action status cannot be found in the current context.');
            }
            // exit if the target status is not active
            if (targetActionStatus.alternateName !== ActionStatusType.ActiveActionStatus) {
                return;
            }
            // reset canSelectSpecialty
            event.target.canSelectSpecialty = null;
        }
    })().then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

export function afterSave(event, callback) {
    (async () => {
        // operate only on actionStatus update
        if (!(event.state === DataObjectState.Update && event.target.hasOwnProperty('actionStatus'))) {
            return;
        }
        // get previous action status
        const previousActionStatus = event.previous && event.previous.actionStatus && event.previous.actionStatus.alternateName;
        // and validate it
        if (previousActionStatus == null) {
            throw new DataError('E_PREVIOUS_STATUS', 'The previous action status of the object cannot be determined.');
        }
        // exit if the previous status is not active
        if (previousActionStatus !== ActionStatusType.ActiveActionStatus) {
            return;
        }
        // find target action status
        const context = event.model.context;
        const targetActionStatus = await context.model('ActionStatusType').find(event.target.actionStatus).getItem();
        // and validate it
        if (targetActionStatus == null) {
            throw new DataError('E_TARGET_STATUS', 'The target action status cannot be found in the current context.');
        }
        // exit if the target status is not completed
        if (targetActionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
            return;
        }
        // create an UpdateSpecialtyAction
        const updateSpecialtyAction = {
            object: event.previous.student,
            specialty: event.previous.specialty,
            initiator: event.previous.id
        };
        // note: This action is auto completed when created as active (which is the default status)
        await context.model('UpdateSpecialtyAction').save(updateSpecialtyAction);
    })().then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
