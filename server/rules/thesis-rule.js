import _ from 'lodash';
import {TraceUtils, LangUtils} from '@themost/common/utils';
import util from "util";
import { QueryExpression } from '@themost/query/query';
import Rule from "./../models/rule-model";

/**
 * @class
 * @augments Rule
 */
export default class ThesisRule extends Rule {
    constructor(obj) {
        super('Rule', obj);
    }
    /**
     * Validates an expression against the current student
     * @param {*} obj
     * @param {function(Error=,*=)} done
     */
    validate(obj, done) {
        try {
            const self = this;
            const context = self.context;
            const students = context.model('Student'), student = students.convert(obj.student), thesis=context.model('StudentThesis');
            if (_.isNil(student)) {
                return done(null, self.failure('ESTUD','Student data is missing.'));
            }
            if (_.isNil(student.getId())) {
                return done(null, self.failure('ESTUD','Student data cannot be found.'));
            }
            const op = this.operatorOf();
            if (_.isNil(op)) {
                return done(null, self.failure('EFAIL','An error occurred while trying to validate rules.','The specified operator is not yet implemented.'));
            }
            //get queryable object completed thesis
            const q = thesis.where('student').equal(student.getId()).and('thesis/status').equal(3).prepare();
            let fnOperator = q[op];
            if (typeof fnOperator !== 'function') {
                return done(null, self.failure('EFAIL','An error occurred while trying to validate rules.', 'The specified operator cannot be found or is invalid.'));
            }
            const sumOf=this.value6;
            let fieldOf;

            switch (sumOf) {
                case "0":
                    fieldOf="sum(thesis/units) as res"; //units
                    break;
                case "3":
                    fieldOf="sum(thesis/ects) as res"; //ects
                    break;
                default:
                    fieldOf="count(thesis) as res";
            }
            if (LangUtils.parseInt(this.checkValues)!==-1 && this.checkValues!=null) {
                //filter with specific thesis type
                const values = this.checkValues.split(',');
                q.where("thesis/type").in(values);
            }

            q.silent().select(fieldOf).first (function(err, result) {
                if (err) {
                    TraceUtils.error(err);
                    return done(null, self.failure('EFAIL', 'An error occurred while trying to validate rules.'));
                }
                // keep result in data and pass this to validationResult
                const data= {
                    "result": result.res,
                    "operator": op,
                    "value1": self.value1,
                    "value2": self.value2
                };
                const q2 = (new QueryExpression().from('a')).select([
                    { res: { $value: LangUtils.parseFloat(result.res) }}
                ]).where('res');
                q2.$fixed = true;
                fnOperator = q2[op];
                fnOperator.apply(q2, [ LangUtils.parseFloat(self.value1), LangUtils.parseFloat(self.value2) ]);
                thesis.context.db.execute(q2, null, function (err, result) {
                    if (err) {
                        return done(err);
                    }
                    else {
                        if (result.length === 1) {
                            self.formatDescription(function(err, message) {
                                if (err) { return done(err); }
                                return done(null, self.success('SUCC', message, null, data));
                            });
                        }
                        else {
                            self.formatDescription(function(err, message) {
                                if (err) { return done(err); }
                                return done(null, self.failure('FAIL',message,null,data));
                            });
                        }
                    }
                });

            });
        }
        catch(e) {
            done(e);
        }
    }

    formatDescription(callback) {
        const op = LangUtils.parseInt(this.ruleOperator);
        let s;
        let description;
        const self=this;
        const sumOf=this.value6;
        let fieldDescription;
        const args=[];

        switch (sumOf) {
            case "0":
                fieldDescription="Units of completed thesis";
                break;
            case "3":
                fieldDescription="ECTS of completed thesis";
                break;
            default:
                fieldDescription="Number of completed thesis";
        }
        switch (op) {
            case 0:s = "%s must be equal to %s"; break;
            case 1:s = "%s must contain '%s'"; break;
            case 2: s = "%s must be different from %s"; break;
            case 3: s = "%s must be greater than %s"; break;
            case 4: s = "%s must be lower than %s"; break;
            case 5: s = "%s must be between %s and %s"; break;
            case 6: s = "%s must start with '%s'"; break;
            case 7: s = "%s must not contain '%s'"; break;
            case 8: s = "%s must be greater or equal to %s"; break;
            case 9: s = "%s must be lower or equal to %s"; break;
        }
        s=this.context.__(s);
        args.push(this.context.__(fieldDescription));
        args.push(this.value1);
        args.push(this.value2);
        for (let i = 0; i < args.length; i++) {
            if (_.isNil(args[i])) {
                args[i]='';
            }
        }
        args.unshift(s);
        s= util.format.apply(this,args);
        this.thesisType(function(err,result) {
            if (err) {
                callback(err)
            }
            description = util.format("%s:%s, %s", self.context.__("Thesis type"), result, s);
            callback(null, description);

        });
    }

    thesisType(callback) {
        try {
            const self=this, value = this.checkValues;
            if (value==="-1" || value == null) {
                callback(null, self.context.__("All types"));
            }
            else {
                const values = value.split(',');
                this.context.model('ThesisType').where('identifier').in(values).select('id', 'name').silent().all(function (err, result) {
                    if (err) {
                        return callback(err);
                    }
                    return callback(null, result.map(function (x) {
                        return util.format('%s', x.name)
                    }).join(', '));
                });
            }
        }
        catch(e) {
            callback(e);
        }
    }
}
