/**
 * @license
 * Universis Project Version 1.0
 * Copyright (c) 2018, Universis Project All rights reserved
 *
 * Use of this source code is governed by an LGPL 3.0 license that can be
 * found in the LICENSE file at https://universis.io/license
 */
import express from 'express';
import {HttpForbiddenError, HttpNotFoundError, Args} from "@themost/common";
import {multerInstance} from './multer';
import {interactiveStudent, tryStudent, validateStudent} from '../middlewares';
import {TraceUtils} from '@themost/common';
import path from 'path';
import {InteractiveUser, postEntityAction} from "@themost/express";
/**
 * @param {ConfigurationBase} configuration
 * @returns Router
 */
function studentsRouter(configuration) {

    let router = express.Router();
    /// set user storage from application configuration
    const upload = multerInstance(configuration);
    upload.storage.getDestination(null, null, (err, destination) => {
        if (err) {
            TraceUtils.error(`Routes: An error occurred while initializing students router.`);
            return TraceUtils.error(err);
        }
        TraceUtils.info(`Routes: Students router starts using "${path.resolve(destination)}" as user storage.`);
    });
    router.post('/:id/messages/send', upload.single('attachment'),async function sendMessage (req, res, next) {
        try {
            // get student
            let student = await req.context.model('Student').where('id').equal(req.params.id).select('id').getTypedItem();
            if (typeof student === 'undefined') {
                // and throw error
                return next(new HttpNotFoundError('Student cannot be found'));
            }
            let result = await new Promise((resolve, reject) => {
                let finalResult;
                return req.context.db.executeInTransaction((cb) => {
                    let message=Object.assign({ }, req.body, {
                        student: student.id
                    });
                    if (message.action)
                    {
                        message.action=parseInt(message.action);
                    }
                    return req.context.model('StudentMessage').save(message).then(message => {
                        // convert message to studentMessage
                        finalResult = req.context.model('StudentMessage').convert(message);
                        if (req.file) {
                            // add attachment to message
                            return finalResult.addAttachment(req.file).then(result => {
                                finalResult.attachments=[];
                                finalResult.attachments.push(result);
                                return cb();
                            });
                        }
                        return cb();
                    }).catch(err => {
                        return cb(err);
                    });
                }, (err)=> {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(finalResult);
                });
            });
            return res.json(result);
        }
        catch (err) {
            return next(err);
        }
    });

    router.get('/me/currentRegistration/effectiveStatus', interactiveStudent(),
        validateStudent(),
        async function getRegistrationEffectiveStatus(req, res, next) {
            try {
                // return current registration status
                const status = await req.student.getCurrentRegistrationStatus();
                // return status
                return res.json(status);
            }
            catch (err) {
                return next(err);
            }
        });

    router.post('/me/messages/:message/markAsRead', interactiveStudent(), async function markAsRead (req, res, next) {
        try {
            const message = req.params.message;
            if (typeof req.student === 'undefined') {
                return next(new HttpForbiddenError());
            }
            /**
             * get studentMessage
             * @type {StudentMessage}
             */
                // check if message exists
            let studentMessage = await req.context.model('StudentMessage')
                    .where('id').equal(message).and('student').equal(req.student.id).getItem();
            // update dateReceived
            if (studentMessage) {
                studentMessage.dateReceived = new Date();
                await req.context.model('StudentMessage').silent().save(studentMessage);
            } else {
                return next(new HttpForbiddenError());
            }
            // return updated student message
            return res.json(studentMessage);
        }
        catch (err) {
            return next(err);
        }
    });
    /**
     * @swagger
     *
     * /api/Students/Me/Exams/{courseExam}/Statistics:
     *   get:
     *    tags:
     *      - Student
     *    description: Returns an array of items which represents statistics about course exam grades
     *    security:
     *      - OAuth2:
     *          - students
     *    parameters:
     *      - in: path
     *        name: courseExam
     *        description: A parameter which represents the id of a course exam
     *        schema:
     *          type: integer
     *        required: true
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              type: object
     *      '404':
     *        description: not found
     *      '403':
     *        description: forbidden
     *      '500':
     *        description: internal server error
     */
    router.get('/me/exams/:courseExam/statistics',
        interactiveStudent(),
        validateStudent(),
        async function getStudentCourseExamStatistics(req, res, next) {
            try {
               // validate student grade
               const exists = await req.context.model('StudentGrade')
                   .where('courseExam').equal(req.params.courseExam)
                   .and('student').equal(req.student.id)
                   .count();
               if (exists) {
                   // get course exam
                   const courseExam = await req.context.model('CourseExam')
                       .where('id').equal(req.params.courseExam)
                       .silent()
                       .getTypedItem();
                   // validate course exam
                   Args.check(courseExam, new HttpNotFoundError('Course exam cannot be found or is inaccessible.'));
                   // get exam statistics
                   const result = await courseExam.getStatistics();
                   return res.json(result);
               }
               return res.json([]);
            }
            catch (err) {
                return next(err);
            }
        });

    router.post('/me/requests/:request/attachments/:attachment/remove', interactiveStudent(), async function removeAttachment (req, res, next) {
        try {
            // get studentRequestAction
            let studentAction = await req.context.model('StudentRequestAction').where('id').equal(req.params.request).select('id').getTypedItem();
            if (typeof studentAction === 'undefined') {
                // and throw error
                return next(new HttpNotFoundError('Student request action cannot be found'));
            }
            studentAction.removeAttachment(req.params.attachment).then(result=>{
                res.json(result);
            }).catch(err=>{
                return next(err);
            });

        } catch (err) {
            return next(err);
        }
    });

    router.post('/me/requests/:request/attachments/add', interactiveStudent(), upload.single('file'),async function addAttachment (req, res, next) {
        try {
            // get studentRequestAction
            let studentAction = await req.context.model('StudentRequestAction').where('id').equal(req.params.request).select('id').getTypedItem();
            if (typeof studentAction === 'undefined') {
                // and throw error
                return next(new HttpNotFoundError('Student request action cannot be found'));
            }
            //assign file attribute
            let file;
            if (req.body && req.body.file)
            {
                file = Object.assign({}, req.body.file, req.file);
            }
            studentAction.addAttachment(file).then(result=>{
                res.json(result);
            }).catch(err=>{
                return next(err);
            });

        } catch (err) {
            return next(err);
        }
    });

    router.post('/me/reports/:report/print',interactiveStudent(), validateStudent() , async function printReport (req, res, next) {
        try {
            const reportTemplate = await req.context.model('ReportTemplate').where('id').equal(req.params.report).getTypedItem();
            if (typeof reportTemplate === 'undefined') {
                return next(new HttpForbiddenError('Report template cannot be found or is inaccessible'));
            }
            req.params.action = 'print'
            return postEntityAction({
                entityActionFrom: 'action',
                from: 'report',
                entitySet: 'ReportTemplates'
            })(req, res, next);
        }
        catch (err) {
            return next(err);
        }
    });
    router.post('/:id/attachments/add', upload.single('file'),async function addAttachment (req, res, next) {
        try {
            // get student
            let student = await req.context.model('Student').where('id').equal(req.params.id).select('id').getTypedItem();
            if (typeof student === 'undefined') {
                // and throw error
                return next(new HttpNotFoundError('Student cannot be found'));
            }
            //assign file attribute
            let file;
            if (req.body && req.body.file)
            {
                file = Object.assign({}, req.body.file, req.file);
            }
            student.addAttachment(file).then(result=>{
                res.json(result);
            }).catch(err=>{
                return next(err);
            });

        } catch (err) {
            return next(err);
        }
    });
    router.post('/:id/attachments/:attachment/remove',async function removeAttachment (req, res, next) {
        try {
            // get student
            let student = await req.context.model('Student').where('id').equal(req.params.id).select('id').getTypedItem();
            if (typeof student === 'undefined') {
                // and throw error
                return next(new HttpNotFoundError('Student cannot be found'));
            }
            student.removeAttachment(req.params.attachment).then(result=>{
                res.json(result);
            }).catch(err=>{
                return next(err);
            });

        } catch (err) {
            return next(err);
        }
    });

    return router;
}

export {studentsRouter};
