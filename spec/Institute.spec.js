import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import random from './random';
import Institute from '../server/models/institute-model';
import Department from '../server/models/department-model';
const executeInTransaction = TestUtils.executeInTransaction;

describe('Institute', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        // disable sql logging
        process.env.NODE_ENV = 'test';
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        // re-enable development env
        process.env.NODE_ENV = 'development';
        return done();
    });

    it('should create institute', async () => {
        await executeInTransaction(context, async ()=> {
            let institute = random.institute();
            await context.model(Institute).silent().insert(institute);
            institute = await context.model(Institute)
                .where('id').equal(institute.id)
                .silent()
                .getItem();
            expect(institute).toBeTruthy();
        });
    });

    it('should update institute', async () => {
        await executeInTransaction(context, async ()=> {
            let institute = random.institute();
            await context.model(Institute).silent().insert(institute);
            institute = await context.model(Institute)
                .where('id').equal(institute.id)
                .silent()
                .getItem();
            institute.alternateName = 'UOL';
            await context.model(Institute).silent().save(institute);
            institute = await context.model(Institute)
                .where('id').equal(institute.id)
                .silent()
                .getItem();
            expect(institute).toBeTruthy();
            expect(institute.alternateName).toBe('UOL');
        });
    });

    it('should remove institute', async () => {
        await executeInTransaction(context, async ()=> {
            let institute = random.institute();
            await context.model(Institute).silent().insert(institute);
            institute = await context.model(Institute)
                .where('id').equal(institute.id)
                .silent()
                .getItem();
            await context.model(Institute).silent().remove(institute);
            institute = await context.model(Institute)
                .where('id').equal(institute.id)
                .silent()
                .getItem();
            expect(institute).toBeFalsy();
        });
    });

    it('should insert institute department', async () => {
        await executeInTransaction(context, async ()=> {
            // insert institute
            let institute = random.institute();
            await context.model(Institute).silent().insert(institute);
            institute = await context.model(Institute)
                .where('id').equal(institute.id)
                .silent()
                .getItem();
            // insert department
            let department = random.department();
            department.organization = institute;
            await context.model(Department).silent().insert(department);
            department = await context.model(Department)
                .where('id').equal(department.id)
                .silent()
                .getItem();
            expect(department).toBeTruthy();
            expect(department.organization).toBe(institute.id);
        });
    });

    it('should try to delete institute', async () => {
        await executeInTransaction(context, async ()=> {
            // insert institute
            let institute = random.institute();
            await context.model(Institute).silent().insert(institute);
            institute = await context.model(Institute)
                .where('id').equal(institute.id)
                .silent()
                .getItem();
            // insert department
            let department = random.department();
            department.organization = institute;
            await context.model(Department).silent().insert(department);
            department = await context.model(Department)
                .where('id').equal(department.id)
                .silent()
                .getItem();
            expect(department).toBeTruthy();
            expect(department.organization).toBe(institute.id);
            // try to remove institute
            await expectAsync((function() {
                return context.model(Institute).silent().remove(institute);
            })()).toBeRejected();
        });
    });

});
